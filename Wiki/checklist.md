## Checklist/roadmap for lawnlogic

### API to:

- ~~login for `admin` using JWT and servant-auth.~~
- create facility by loggedIn admin.
- update the facility.
- create groupFacilty containing at least 1 facility.
- add/remove facilities to/from group.
- update all the facility attributes using groupFacility.
- delete the groupFacility.
- register new users.
- login users.
- search facilities by name.
- search facilites by address.
- find available slots for given facility.
- search facilities which are available in given time-slot.
- create the swagger UI for all the APIs. 
- create CI/CD pipeline to build and run tests for develop branch. 
- get a life (Optional).
- Mechanism to automate migrating process of schema.
