Language: haskell
    compiler: ghc-9.4.8
    web-framework: servant
database: postgresql-14.11
operating-system: ubuntu-22.04 lts (jammy jellyfish)

- For hashing anything, I will be using `bCrypt` defaulting to 10 rounds.

libraries used:
- aeson
- co-log
- beam
- postgresql-simple (partially)
- servant
- text
- bytestring
- mtl
- transformers
- passoword (will used password instead of `crypton`)

## Learning Resource

- [beam-documentation](https://haskell-beam.github.io/beam/)
- [auth-servant-example](https://github.com/Josh-Miller/servant-jwt-starter/tree/master)
- [auth-servant-example2](https://github.com/haskell-servant/servant/tree/master/servant-auth/servant-auth-server)
- This project has been highly inspired by [this](https://github.com/imborge/hsweb/tree/master/%7B%7Bcookiecutter.project_slug%7D%7D)
- [hoistServer to pass in reader](https://hackage.haskell.org/package/servant-server-0.20/docs/Servant-Server.html#v:hoistServer)
