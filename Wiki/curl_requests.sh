curl --request GET \
  --url http://localhost:8081/top_rated_facility/footballTurf

  curl --request GET \
  --url http://localhost:8081/find_slot \
  --header 'Content-Type: application/json' \
  --data '{
	"facilityId" : 3,
	"date" : "2024-06-14"
}'

curl --request POST \
  --url http://localhost:8081/user/add_waitlist \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2NyZWF0ZWRBdCI6IjIwMjQtMDYtMTBUMTc6MjE6MjQuNDQyMjg4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA2LTEwVDE3OjIxOjI0LjQ0MjI4OFoiLCJfdXNlckVtYWlsIjoidHVzaGFyQGdtYWlsLmNvbSIsIl91c2VySWQiOjMsIl91c2VyTmFtZSI6InR1c2hhciIsIl91c2VyUGFzc3dvcmQiOiIkMmIkMTAkWWQ5Q1VtN0xJYWMvVjE1TG5LQkVNTzFzLnZjajUxOEJFazNQd0RQQzlrRlhZY2tUbEJMSS4ifX0._dc_J5d7aoQhFf_yWgkwTjuJgCfv7pvQ62V8IizzzaWPCZ0G0xciZgRl_xJAUK4s-2OmYSiVAG_y_10zNCDslA' \
  --header 'Content-Type: application/json' \
  --data '{
	"facilityId" : 1,
	"startTime" : "2011-10-10T15:48:00.000+09:00",
	"endTime": "2011-10-10T17:48:00.000+09:00"
}'

curl --request POST \
  --url http://localhost:8081/user/verify_booking/17 \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2NyZWF0ZWRBdCI6IjIwMjQtMDYtMTBUMTc6MjE6MjQuNDQyMjg4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA2LTEwVDE3OjIxOjI0LjQ0MjI4OFoiLCJfdXNlckVtYWlsIjoidHVzaGFyQGdtYWlsLmNvbSIsIl91c2VySWQiOjMsIl91c2VyTmFtZSI6InR1c2hhciIsIl91c2VyUGFzc3dvcmQiOiIkMmIkMTAkWWQ5Q1VtN0xJYWMvVjE1TG5LQkVNTzFzLnZjajUxOEJFazNQd0RQQzlrRlhZY2tUbEJMSS4ifX0.QMmTxpmbNRVtDhMW7e5Jw_RPRGHALPCuMl7jo-_hHub-InfzZPqznjRiI-89h-n1AXutX-QVF522kcgm-pREAA' \
  --header 'Content-Type: application/json' \
  --data '{
	"otp" : 4321
}'

curl --request DELETE \
  --url http://localhost:8081/user/cancel_booking/16 \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2NyZWF0ZWRBdCI6IjIwMjQtMDYtMTBUMTc6MjE6MjQuNDQyMjg4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA2LTEwVDE3OjIxOjI0LjQ0MjI4OFoiLCJfdXNlckVtYWlsIjoidHVzaGFyQGdtYWlsLmNvbSIsIl91c2VySWQiOjMsIl91c2VyTmFtZSI6InR1c2hhciIsIl91c2VyUGFzc3dvcmQiOiIkMmIkMTAkWWQ5Q1VtN0xJYWMvVjE1TG5LQkVNTzFzLnZjajUxOEJFazNQd0RQQzlrRlhZY2tUbEJMSS4ifX0.yeObksrAX7rqqiZRPcxzsjbm1AmwkftkUq98CaraoR9UsqUgEIPFBww3fPBs4q1SjoFcUmhFxOZrxjyYMMIVZA'

curl --request POST \
  --url http://localhost:8081/user/pay_booking \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2NyZWF0ZWRBdCI6IjIwMjQtMDYtMTBUMTc6MjE6MjQuNDQyMjg4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA2LTEwVDE3OjIxOjI0LjQ0MjI4OFoiLCJfdXNlckVtYWlsIjoidHVzaGFyQGdtYWlsLmNvbSIsIl91c2VySWQiOjMsIl91c2VyTmFtZSI6InR1c2hhciIsIl91c2VyUGFzc3dvcmQiOiIkMmIkMTAkWWQ5Q1VtN0xJYWMvVjE1TG5LQkVNTzFzLnZjajUxOEJFazNQd0RQQzlrRlhZY2tUbEJMSS4ifX0.QMmTxpmbNRVtDhMW7e5Jw_RPRGHALPCuMl7jo-_hHub-InfzZPqznjRiI-89h-n1AXutX-QVF522kcgm-pREAA' \
  --header 'Content-Type: application/json' \
  --data '{
	"bookingId":17,
	"payingAmount":1000
}'

curl --request POST \
  --url http://localhost:8081/admin/set_holiday/facility \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2NyZWF0ZWRBdCI6IjIwMjQtMDYtMTBUMTc6MjE6MjQuNDQyMjg4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA2LTEwVDE3OjIxOjI0LjQ0MjI4OFoiLCJfdXNlckVtYWlsIjoidHVzaGFyQGdtYWlsLmNvbSIsIl91c2VySWQiOjMsIl91c2VyTmFtZSI6InR1c2hhciIsIl91c2VyUGFzc3dvcmQiOiIkMmIkMTAkWWQ5Q1VtN0xJYWMvVjE1TG5LQkVNTzFzLnZjajUxOEJFazNQd0RQQzlrRlhZY2tUbEJMSS4ifX0.FEHoj-n5KmmeLnj9hXSytP9ftA_nmZuXk77Wgb_LA1wQ4-TnrNUl5ZVrmofeyVIEgAtrOGypTHEd3s4UaIBLpw' \
  --header 'Content-Type: application/json' \
  --data '{
	"facilityId" : 1,
	"startDate" : "2011-09-10T14:48:00.000+09:00",
	"endDate" : "2011-11-10T14:48:00.000+09:00",
	"typeOfHoliday" : "holiday"
}'

curl --request GET \
  --url http://localhost:8081/user/profile \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2NyZWF0ZWRBdCI6IjIwMjQtMDYtMTBUMTc6MjE6MjQuNDQyMjg4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA2LTEwVDE3OjIxOjI0LjQ0MjI4OFoiLCJfdXNlckVtYWlsIjoidHVzaGFyQGdtYWlsLmNvbSIsIl91c2VySWQiOjMsIl91c2VyTmFtZSI6InR1c2hhciIsIl91c2VyUGFzc3dvcmQiOiIkMmIkMTAkWWQ5Q1VtN0xJYWMvVjE1TG5LQkVNTzFzLnZjajUxOEJFazNQd0RQQzlrRlhZY2tUbEJMSS4ifX0.UJfaZMpyp12s8YXV8EqMt2EvjF5BSqBr4dwtsAvdnezELhWo_m_9EaT5C6fmSOwLZPY5YMO_UaYVMLNuNDPwJw'

curl --request POST \
  --url http://localhost:8081/user/book_facility \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2NyZWF0ZWRBdCI6IjIwMjQtMDYtMTBUMTc6MjE6MjQuNDQyMjg4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA2LTEwVDE3OjIxOjI0LjQ0MjI4OFoiLCJfdXNlckVtYWlsIjoidHVzaGFyQGdtYWlsLmNvbSIsIl91c2VySWQiOjMsIl91c2VyTmFtZSI6InR1c2hhciIsIl91c2VyUGFzc3dvcmQiOiIkMmIkMTAkWWQ5Q1VtN0xJYWMvVjE1TG5LQkVNTzFzLnZjajUxOEJFazNQd0RQQzlrRlhZY2tUbEJMSS4ifX0.4nx91Z6d_tXDoGIIBeov6iG45u8bBv6h6Uj1uhBwnCjFCbw4Xv3snb97_pPYakKFAYlvm4PLr6GaAmy13v7gMw' \
  --header 'Content-Type: application/json' \
  --data '{
	"facilityId" : 1,
	"startTime" : "2011-10-10T15:48:00.000+09:00",
	"endTime": "2011-10-10T17:48:00.000+09:00"
}'

curl --request POST \
  --url http://localhost:8081/user/login \
  --header 'Content-Type: application/json' \
  --data '{
	"loginUserEmail" : "tushar@gmail.com",
	"loginUserPassword" : "foobar"
}'

curl --request POST \
  --url http://localhost:8081/user/register \
  --header 'Content-Type: application/json' \
  --data '{
	"registerUserName" : "tushar2",
	"registerUserEmail" : "tushar2@gmail.com",
	"registerUserPassword" : "foobar",
	"registerUserConfirmPassword" : "foobar"
}'

curl --request GET \
  --url http://localhost:8081/facilities

curl --request GET \
  --url http://localhost:8081/facilities 

curl --request POST \
  --url http://localhost:8081/admin/group/3/all_update \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2FkbWluRW1haWwiOiJwZXRlckBsYXdubG9naWMuY29tIiwiX2FkbWluSWQiOjEsIl9hZG1pbk5hbWUiOiJwZXRlciIsIl9hZG1pblBhc3N3b3JkIjoiJDJhJDEwJEl2S2lRZFBKM0JiM0VwZ2ZUR3gxZ09WLlN2cVhoNTB5RXU1TE5nSDRDdUUzYnc2MjV6SmptIiwiX2NyZWF0ZWRBdCI6IjIwMjQtMDUtMzFUMTY6MjQ6NTEuNDQxMzQ4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA1LTMxVDE2OjI0OjUxLjQ0MTM0OFoifX0.K3mQzzUT4VbP0PRnjTiDdTlFmNi9jxyB9xomWTFVfIr4T6WNb5cIbN69AZw6wfCx4cWbayMXxWGY3Y6pwEWtQg' \
  --header 'Content-Type: multipart/form-data' \
  --form rentPerMinHour=500

curl --request DELETE \
  --url http://localhost:8081/admin/delete_facility/4 \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2FkbWluRW1haWwiOiJwZXRlckBsYXdubG9naWMuY29tIiwiX2FkbWluSWQiOjEsIl9hZG1pbk5hbWUiOiJwZXRlciIsIl9hZG1pblBhc3N3b3JkIjoiJDJhJDEwJEl2S2lRZFBKM0JiM0VwZ2ZUR3gxZ09WLlN2cVhoNTB5RXU1TE5nSDRDdUUzYnc2MjV6SmptIiwiX2NyZWF0ZWRBdCI6IjIwMjQtMDUtMzFUMTY6MjQ6NTEuNDQxMzQ4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA1LTMxVDE2OjI0OjUxLjQ0MTM0OFoifX0.4RBa7s5WsQtvxL01xN7uuT5LhIPsr3KZVJ06NUonLmpVjhX2ij87gJlbQ_4n5evZAihh4krR4WOZ0ks-RPJiVA' \

curl --request DELETE \
  --url http://localhost:8081/admin/delete_group/1 \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2FkbWluRW1haWwiOiJwZXRlckBsYXdubG9naWMuY29tIiwiX2FkbWluSWQiOjEsIl9hZG1pbk5hbWUiOiJwZXRlciIsIl9hZG1pblBhc3N3b3JkIjoiJDJhJDEwJEl2S2lRZFBKM0JiM0VwZ2ZUR3gxZ09WLlN2cVhoNTB5RXU1TE5nSDRDdUUzYnc2MjV6SmptIiwiX2NyZWF0ZWRBdCI6IjIwMjQtMDUtMzFUMTY6MjQ6NTEuNDQxMzQ4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA1LTMxVDE2OjI0OjUxLjQ0MTM0OFoifX0.4RBa7s5WsQtvxL01xN7uuT5LhIPsr3KZVJ06NUonLmpVjhX2ij87gJlbQ_4n5evZAihh4krR4WOZ0ks-RPJiVA'

curl --request PUT \
  --url http://localhost:8081/admin/update_group/1/add_facility/18 \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2FkbWluRW1haWwiOiJwZXRlckBsYXdubG9naWMuY29tIiwiX2FkbWluSWQiOjEsIl9hZG1pbk5hbWUiOiJwZXRlciIsIl9hZG1pblBhc3N3b3JkIjoiJDJhJDEwJEl2S2lRZFBKM0JiM0VwZ2ZUR3gxZ09WLlN2cVhoNTB5RXU1TE5nSDRDdUUzYnc2MjV6SmptIiwiX2NyZWF0ZWRBdCI6IjIwMjQtMDUtMzFUMTY6MjQ6NTEuNDQxMzQ4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA1LTMxVDE2OjI0OjUxLjQ0MTM0OFoifX0.a_z7T06H8APkGg7lcNqlz8W0Zu_UJ3QnK7l8w2fRNXM5jp8KfpSeLeIcGHXtPpqPgn5c0f1GQnfshuiqhuts8Q'

curl --request PUT \
  --url http://localhost:8081/admin/update_group/1/remove_facility/2 \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2FkbWluRW1haWwiOiJwZXRlckBsYXdubG9naWMuY29tIiwiX2FkbWluSWQiOjEsIl9hZG1pbk5hbWUiOiJwZXRlciIsIl9hZG1pblBhc3N3b3JkIjoiJDJhJDEwJEl2S2lRZFBKM0JiM0VwZ2ZUR3gxZ09WLlN2cVhoNTB5RXU1TE5nSDRDdUUzYnc2MjV6SmptIiwiX2NyZWF0ZWRBdCI6IjIwMjQtMDUtMzFUMTY6MjQ6NTEuNDQxMzQ4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA1LTMxVDE2OjI0OjUxLjQ0MTM0OFoifX0.WgXw5IxsTeWNyr_6pU5RtAKXtNduij7OMJOfKST8HA6pyXzEzFDAeEgFuH-6g22rZ6geu5Bppc5B5_useXzw7w'

curl --request POST \
  --url http://localhost:8081/admin/create_group \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2FkbWluRW1haWwiOiJwZXRlckBsYXdubG9naWMuY29tIiwiX2FkbWluSWQiOjEsIl9hZG1pbk5hbWUiOiJwZXRlciIsIl9hZG1pblBhc3N3b3JkIjoiJDJhJDEwJEl2S2lRZFBKM0JiM0VwZ2ZUR3gxZ09WLlN2cVhoNTB5RXU1TE5nSDRDdUUzYnc2MjV6SmptIiwiX2NyZWF0ZWRBdCI6IjIwMjQtMDUtMzFUMTY6MjQ6NTEuNDQxMzQ4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA1LTMxVDE2OjI0OjUxLjQ0MTM0OFoifX0.JMT0Kl7Jzb2wCpbtFzPs72ty6bfby0ltz4474h-dt03muCiGYgD5q1gClahC_UNQiLN7iq0_kRujkayZEjSXlQ' \
  --header 'Content-Type: application/json' \
  --data '{
	"groupName" : "Sports complex abc",
	"facilityIds" : [3,1,2]
}'

curl --request PUT \
  --url http://localhost:8081/admin/update_facility/23 \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2FkbWluRW1haWwiOiJwZXRlckBsYXdubG9naWMuY29tIiwiX2FkbWluSWQiOjEsIl9hZG1pbk5hbWUiOiJwZXRlciIsIl9hZG1pblBhc3N3b3JkIjoiJDJhJDEwJEl2S2lRZFBKM0JiM0VwZ2ZUR3gxZ09WLlN2cVhoNTB5RXU1TE5nSDRDdUUzYnc2MjV6SmptIiwiX2NyZWF0ZWRBdCI6IjIwMjQtMDUtMzFUMTY6MjQ6NTEuNDQxMzQ4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA1LTMxVDE2OjI0OjUxLjQ0MTM0OFoifX0.m5jD5yuvpPZ4aYhLRue6x9xNPe3GsBpED8KguMt5s1PlbdXx46gbZYerxVpTZUCplU40Yzvirbp-8iyVtDZhBw' \
  --header 'Content-Type: multipart/form-data' \
  --form facilityName=tushar5 \
  --form minBookHour=01:00 \
  --form facilityType=footballTurf \
  --form facilityAddress=Pune \
  --form rentPerMinHour=101 \
  --form facilityStartTime=08:00 \
  --form facilityEndTime=23:00 \
  --form facilityImage=@/home/user/Pictures/Screenshots/3.png

curl --request POST \
  --url http://localhost:8081/user/add_rating/1 \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2NyZWF0ZWRBdCI6IjIwMjQtMDYtMTBUMTc6MjE6MjQuNDQyMjg4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA2LTEwVDE3OjIxOjI0LjQ0MjI4OFoiLCJfdXNlckVtYWlsIjoidHVzaGFyQGdtYWlsLmNvbSIsIl91c2VySWQiOjMsIl91c2VyTmFtZSI6InR1c2hhciIsIl91c2VyUGFzc3dvcmQiOiIkMmIkMTAkWWQ5Q1VtN0xJYWMvVjE1TG5LQkVNTzFzLnZjajUxOEJFazNQd0RQQzlrRlhZY2tUbEJMSS4ifX0.POr5Ju6bNu9OXUvLr807pN4fHfSQ8qPvpQ_lCeNDbq_2Xqu1-U5opPs1qkeK75GYsAXi8Vk4ATdy3P8dOG4wIg' \
  --header 'Content-Type: application/json' \
  --data '{
	"rating":7,
	"review":"good maintainance!"
}'

curl --request GET \
  --url http://localhost:8081/admin/dashboard \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2FkbWluRW1haWwiOiJwZXRlckBsYXdubG9naWMuY29tIiwiX2FkbWluSWQiOjEsIl9hZG1pbk5hbWUiOiJwZXRlciIsIl9hZG1pblBhc3N3b3JkIjoiJDJhJDEwJEl2S2lRZFBKM0JiM0VwZ2ZUR3gxZ09WLlN2cVhoNTB5RXU1TE5nSDRDdUUzYnc2MjV6SmptIiwiX2NyZWF0ZWRBdCI6IjIwMjQtMDUtMzFUMTY6MjQ6NTEuNDQxMzQ4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA1LTMxVDE2OjI0OjUxLjQ0MTM0OFoifX0.1FAya1e1slMlBweUspMS1MzbaT-XcKvldMepeGqTtgu5Hxx83SAtxtxM3DJom204xqTj2us8lPV3Xeztj8rd2w' \
  --data '{
	"adminLoginEmail": "tushar@gmail.com",
	"adminLoginPassword": "foobar1"
}'

curl --request POST \
  --url http://localhost:8081/admin/login \
  --header 'Content-Type: application/json' \
  --data '{
	"adminLoginEmail": "peter@lawnlogic.com",
	"adminLoginPassword": "foobar1"
}'

curl --request POST \
  --url http://localhost:8081/admin/add_facility \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2FkbWluRW1haWwiOiJwZXRlckBsYXdubG9naWMuY29tIiwiX2FkbWluSWQiOjEsIl9hZG1pbk5hbWUiOiJwZXRlciIsIl9hZG1pblBhc3N3b3JkIjoiJDJhJDEwJEl2S2lRZFBKM0JiM0VwZ2ZUR3gxZ09WLlN2cVhoNTB5RXU1TE5nSDRDdUUzYnc2MjV6SmptIiwiX2NyZWF0ZWRBdCI6IjIwMjQtMDUtMzFUMTY6MjQ6NTEuNDQxMzQ4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA1LTMxVDE2OjI0OjUxLjQ0MTM0OFoifX0.S-jHBYtgtqcJMnTVrDneiTPd4w3_NFi2ytGqxzOkg3NY6wHHqvA4LQBMrcogMX5TxsZhTs73wzIkF_UCEizgsA' \
  --header 'Content-Type: multipart/form-data' \
  --form facilityName=tushar3 \
  --form minBookHour=01:00 \
  --form facilityType=footballTurf \
  --form facilityAddress=Pune \
  --form rentPerMinHour=100 \
  --form facilityStartTime=08:00 \
  --form facilityEndTime=23:00

curl --request GET \
  --url http://localhost:8081/search_facility \
  --header 'Content-Type: application/json'

curl --request GET \
  --url http://localhost:8081/admin/booking_statistics \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2FkbWluRW1haWwiOiJwZXRlckBsYXdubG9naWMuY29tIiwiX2FkbWluSWQiOjEsIl9hZG1pbk5hbWUiOiJwZXRlciIsIl9hZG1pblBhc3N3b3JkIjoiJDJhJDEwJEl2S2lRZFBKM0JiM0VwZ2ZUR3gxZ09WLlN2cVhoNTB5RXU1TE5nSDRDdUUzYnc2MjV6SmptIiwiX2NyZWF0ZWRBdCI6IjIwMjQtMDUtMzFUMTY6MjQ6NTEuNDQxMzQ4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA1LTMxVDE2OjI0OjUxLjQ0MTM0OFoifX0.2rLPEBrqsCmUgxm_eUivN7WHCHAU68pXDSBcgpIpHvbOFC5cbMsGvtQN08RslWBgdFm8dPHlIbl11aJXMcYS2A' \
  --header 'User-Agent: insomnia/9.2.0'
#{
#	"mostBookedFacility": "tushar3",
#	"mostCancelledFacility": null,
#	"mostUnusedFacility": "tushar5",
#	"peakBookingHours": [
#		"2024-06-14T07:30:00Z",
#		"2024-06-15T06:30:00Z",
#		"2024-06-14T09:30:00Z"
#	]
#}

curl --request GET \
  --url http://localhost:8081/admin/export_statistics \
  --header 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJkYXQiOnsiX2FkbWluRW1haWwiOiJwZXRlckBsYXdubG9naWMuY29tIiwiX2FkbWluSWQiOjEsIl9hZG1pbk5hbWUiOiJwZXRlciIsIl9hZG1pblBhc3N3b3JkIjoiJDJhJDEwJEl2S2lRZFBKM0JiM0VwZ2ZUR3gxZ09WLlN2cVhoNTB5RXU1TE5nSDRDdUUzYnc2MjV6SmptIiwiX2NyZWF0ZWRBdCI6IjIwMjQtMDUtMzFUMTY6MjQ6NTEuNDQxMzQ4WiIsIl91cGRhdGVkQXQiOiIyMDI0LTA1LTMxVDE2OjI0OjUxLjQ0MTM0OFoifX0.1tPlQbXIlQr7nddGTHHZ4O4gvfWsecircZE6flZy1GRFkISlMxHvW09dEaMKCXFzLm2tsI4s_wTMYwjqXQNiqg' \
  --header 'User-Agent: insomnia/9.2.0'

# mostBookedFacility,mostCancelledFacility,mostUnusedFacility,peakBookingHours
# tushar3,NULL,tushar2,"[2011-10-10 06:30:00 UTC,2024-06-15 06:30:00 UTC,2024-06-14 07:30:00 UTC,2024-06-14 09:30:00 UTC]"
