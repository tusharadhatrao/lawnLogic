## Authentication

### Admin login

* In order to login for admin, dev should hit the get request with `admin/login` 
  endpoint and pass the json body of the type `adminLoginData`. In return dev will
  get the jwt token, that dev should pass as `bearer token` in order to hit admin
  protected routes.

* E.g Getting jwt token after hitting `admin/login` endpoint. 
      ![alt](admin_login.png)

* Hitting `admin/dashboard` endpoint using bearer token.
    ![alt](admin_dashboard.png)