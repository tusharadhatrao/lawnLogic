- 03/06/2024
Adding add facility functionality, it will come under admin protected routes. 
Required checks:
    1. Check the database if it no other records contains the same name (facility name should be unique).
    2. File image should not exceed 250 KB size.

- 12/06/2024
1. In the bookings relations, having field `book_for_how_many_hours`, doesn't make much sense.
   Also, it is causing a lot of problem since, I have to deal with 2 different types, 
   startTime (UTCTime/Timestamp) and howManyHours (TimeOfDay/Time). Instead there should be a endTime
   with type as (UTCTime/Timstamp). Making that change now in the database, as well changing the logic
   in the handler as well. One thing to also make sure, is the difference between startTime/endTime should
   be a modulo of min_book_hour. for e.g if min_book_hour for a certain facility is 1:30 Hours, the
   slotting should be 1:30,3:00,4:30 etc.
2. Implement payment API, It will take bookingId and amount. Handler will check if the bookingId exist or not.
   If yes, the corresponding user is correct or not and amount is correct or not. After all this, it will
   update the booking_status to `confirm`. Adding two fields in booking relation. OTP and isVerified. 
3. OTP Verification API, It will take bookingId and OTP. Handler will check if the bookingId exist or not.
   If yes, the corresponding user is correct or not and OTP is correct or not. After all this, it will
   update the isVerified to `True`. Also, it should only verify the opt, 10 minutes before the start_time of
   boooked slot.
4. Waitlist API. User can book the slot of a facility, which is already booked by someone else. In that case,
   the user will be added to the waitlist. If the booking is cancelled by the user, 
   the first user in the waitlist will be notified and the booking will be confirmed for him.
5.  Add cancel booking API, The checks that are required are, whether the user is same or not,
    booking exist or not. Once the booking has been cancelled. A query should be fired to check
    if any waitlist exist for perticular facility within the cancelled booking range. If yes, It should
    be added in the booking section with booking_status as pending. When cancelling the booking, only
    the status should change to cancelled. If waitingList got adding, the corrosponding waiting list
    record should be deleted from the db.
6.  Add User rating and review. There shall be a table in the database called `ratings`. In which fields such
    as rating_id,user_id,facility_id,rating (0-10),review (optional),createdAt,updatedAt. 
    (user_id,facility_id) should be unique and indexed. An API for users to post review of the facility.
    Checks which needs to be done are, facility should exist, and user must have visited that facility
    at least once (meaning in the booking records, there should be at least one record with is_verified).
7.  Add Search facility. The search API would be an unprotected API. It will take a custom that type would
    contain a text field called `searchTerm`, will use this searchTerm to do a TSVector/TSQuery search against
    facility's name and address. This `searchTerm` would be a MaybeType, if no searchTerm is provided, it shall
    provide all facilities. This Custom type should also contain various other parameters which would also be
    optional (wrapped in Maybe), such as FaciltyType (list of facilityTypes). Available timeslots (if possible
    it shall order the facilities as per order of nearest timestamps.).
8. Adding test cases, I will be using `hspec-wai` for testing. It will take the config (database creds) from 
   root's `appSetting.json`. In the CI/CD pipline, before testing it should initialize a temporary database
   with sample values.
9. Find available slots of the given facility (facilities?). In order to find the available timeslots, we will
   information such as minBookHour,facilityStartTime and facilityEndTime also the time where facility is booked.
   We shall also take an extra parameter from user which would be the day on which he/she wants to check the available
   timeslots.
10. Get top 5 rated facilities of the given facilityType
11. Get Statistics of facilities, such as most rated,least rating,top booked,least booked,peak booking hours
    ,most cancelled.
12. Export facility Data in JSON/Excel/CSV
13. Booking subscription, to book the subscription, the request will take:
    1. FacilityId
    2. UserId (from auth)
    3. StartDate (date)
    3. EndDate  (date)
    3. startTime (time)
    4. endTime (time)
    System will check if facilityId exists or not. System will find all the timestamps (start and end) 
    based on startDate,endDate and timeSlot. The startDate and endDate


-   suggest similar facility if given facility is already booked. The similar facility must have
    same facility type and is available at given booking hours.

-   Move all tables to schema.
-   Add concurrency (async,atomically)
-   Add logs
-   Add Test cases.
-   CI/CD pipelie
-   Swagger UI.
-   Possible hosting?

Notes:
- One important thing I realised is that while using `runSelectReturningOne`, if there are multiple rows, 
  it will returning nothing despite the fact that records does exist in the table. This problem occured during
  `checkIfUserHasUsedFacility` function. I had to change the function to `runSelectReturningFirst` to make it work.
- Should have used [chronos](https://hackage.haskell.org/package/chronos-1.1.6.1) instead of time.
