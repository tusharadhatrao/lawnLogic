// in this documentation, I have used camelcasing while writing actual schema,
// the fields should have snake casing.

// The schema is design with keeping postgresql-14.11 in mind.

## about role
The role which would be used in the system, shall not have
access of delete records and have limited access 
(only certain relations only) of update.

```sql
admin {
     adminId        serial (pKey)
   , adminName      varchar(255) not NULL
   , adminEmail     varchar(255) unique not null
   , adminPassword  text         // hashed value
   , createdAt      timestampz
   , updatedAt      timestampz
}

users {
      userId        serial (pKey)
    , userName      varchar(255) not NULL
    , userEmail     varchar(255) unique not null
    , userPassword  password        //hashed
    , createdAt     timestampz
    , updatedAt     timestampz
}

facilityGroup {
      groupID     serial (pKey)
    , groupName   varchar(255) not NULL
  , createdAt        timestampz
  , updatedAt        timestampz
}

facility {
    facilityId       serial (pKey)
  , facilityName     varchar(255) not NULL
  , facilityType     Enum (tennisCourt,footballTurf,badmintonCourt)
  , minBookHour      time
  , facilityAddress  varchar(255) not null
  , facilityImages   varchar(255) array
  , groupId          int (fKey facilityGroup) // nullable
  , createdAt        timestampz
  , updatedAt        timestampz
}

bookings {
    bookingId           serial (PKey)
  , userId              int (fKey users)
  , facilityId          int (fKey facility)
  , dateTime            timestampz
  , bookForHowManyHours time
  , bookingStatus       enum (pending,confirmed,cancelled,revoked)
  , createdAt           timestampz
  , updatedAt           timestampz
  // the tuple of (userId,facilityId,dateTime) should be unique/pKey?
}

holidyMaintainsDays {
      facilityId int (fKey facility)
    , startDate  timestampz
    , endDate    timestampz
    , typeOfHolidy enum (holiday,maintains)
}

validatingTokens {
     token  int // ranging 1000 to 9999
   , bookingId int (fKey booking)
   , createdAt timestamptz
   , updatedAt timestamptz
}

waitlist {
    waitListId serial pKey
  , userId     int fKey users
  , facilityId int fKey facility
  , prefferedStartDateTime timestamptz
  , prefferedEndDateTime timestamptz
  , howManyHoursYouWant  time
  , createdAt timestampz
  , updatedAt timestampz
}


booking_subscription {
     subscriptionId serial pkey
   , userId    int fKey users
   , facilityId int fKey facility
   , startDateTimeOfSub timestamptz
   , recurringDays      date
}

```

## Adding/Removing relations in database.

At path `/Scripts/DB/baseline.sql` would be baseline database. If
any change required to be made in database, developer should create
a new .sql file named v`i`.sql for e.g v0.sql, v1.sql...v`n`.sql 
that can imported into the database after baseline.sql and all
v<i-1>.sql files.

### footnotes
- `Time` alone is equivalent to time without time zone.
-  Reason for using timestampz is because it can easily
   marshalled in to haskell type.
- `minBookHour` is minimum amount/blocks of time for which facility can be 
   booked (e.g. 30mins/1hr).
- `facilityImages` would contain array of paths (possibly storage bucket 
   paths) of images.
- `facilityGroup` is logical grouping of facilities to perform mass updation 
   in facilites by admins.
-  Facilities part of the same group must share some attributes (e.g. 
   all can be booked in 1hr slots).
   A trigger needs to be created to avoid adding a facility in a group
   that might not have same attributes as other members.
-  `holidyMaintainsDays`, reason for using `timestampz`, the time can be as
    small as few hours as well as can as long as few days.
-  possible trigger, maintainance/holiday cannot be applied on the 
   day/date which has a confirmed booking status on same facility. Admin
   should revoke (refund) respective booking first. 
-  trigger, user cannot book facility during maintainance/holiday time.
-  there should be concept of offhours for each facility, for example
   this facility will be open from 10 am to 9 pm only, meaning
   it will be closed from 9:01 pm to 9:59 am.
-  token can only be validated 10 minute before the booked time.
-  Possible trigger, if any booking gets cancelled and there
   exists a waitlist in the time, users will get the notification
   to confirm the booking, this can be handled at haskell side.
