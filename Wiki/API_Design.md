### API Design

This is the api structure design for facility management system. The design may change as we move 
forward in the project. We will faithfully try to follow [this](https://github.com/byrondover/api-guidelines/blob/master/Guidelines.md).

- All the apis that requires `admin` authorization, for e.g `addFacility` shall prefixed with `admin`
  keyword.
- `None` keyword in Request section, represents `NoBody`.
- We Shall follow `snake_casing`, during end-points.

1. POST http://localhost/admin/add_facility
 
    Request  - JSON `Facility`

    Response - JSON ()

2. PUT http://localhost/admin/update_facility/<facility_id>

    Request  - JSON `Facility`
    
    Response - JSON ()

3. DELETE http://localhost/admin/delete_facility/<facility_id>

    Request  - None

    Response - JSON ()

4. POST http://localhost/admin/create_group

    Request  - JSON `Group`

    Response - JSON ()

5. PUT http://localhost/admin/update_group/add_facility/<facility_id>

    Request  - None

    Response - JSON ()

6. PUT http://localhost/admin/update_group/remove_facility/<facility_id>

    Request  - None

    Response - JSON ()

7. DELETE http://localhost/admin/delete_group/<group_id>

    Request - None

    Response - JSON ()

8. POST http://locahost/admin/set_holiday/facility/<facility_id>
    
    Request - JSON `Date Duration`

    Response - JSON ()
    Note:  Holiday/maintaince days can be setup by admin. Admin can only setup days for the maintainance/holiday
       and not any specific timeslot. There is a constraint on admins, where admins cannot set maintaince/holiday       on days where, there is a confirm booking. Admin must mannually refund the booking if he/she wants
       to setup holiday/maintainance on that perticular date.

9. POST http://locahost/admin/set_holiday/group/<group_id>

    Request - JSON `Date Duration`

    Response - JSON ()

10. DELETE http://locahost/admin/remove_holiday/<holiday_id>

    Request - None

    Response - JSON ()

11. DELETE http://locahost/admin/remove_holiday/<holiday_id>

    Request - None

    Response - JSON ()

12. PUT http://localhost/admin/group

    -- Facility attributes that needs to be `mass updated` 
 
    Request - JSON `Facility` 

    Response - JSON ()

13. GET http://locahost/facilities

    Request - None

    Response - JSON `List[Facility]`

14. GET http://locahost/facility/<facility_id>
    
        Request - None
    
        Response - JSON `Facility`

15. POST http://locahost/user/book_facility/<facility_id>

    Request - JSON `Booking`

    Response - JSON `Booking_OTP`

16. POST http://locahost/user/cancel_booking/<booking_id>
    
        Request - JSON `Canceling_reason`
    
        Response - JSON ()

17. GET http://locahost/user/bookings

    Request - None

    Response - JSON `List[Booking]`

18. GET http://locahost/user/booking/<booking_id>

    Request - None

    Response - JSON `Booking`

19. GET http://locahost/user/booking/<booking_id>/status

    Request - None

    Response - JSON `BookingStatus`

20. POST http://locahost/user/booking/activate

    Request - JSON `Booking_OTP`

    Response - JSON ()

21. Get http://localhost/search_facility

    Request - JSON `SearchQuery`

    Response - JSON `List[Facility]`

22. Get http://localhost/search_facility_by_time

    Request - JSON `time_range`

    Response - JSON `List[Facility]`

23. POST http://localhost/user/add_facility_rating/<facility_id>

    Request - JSON `FacilityRatuings`

    Response - JSON ()

24. GET http://localhost/facility_ratings/<facility_id>

    Request - None

    Response - JSON `List[FacilityRatings]`

25. GET http://localhost/admin/facility/export/<facility_id>/<file_type>

    Request - None

    Response - File

