## Best practices that needs to be followed.

- I will be breifly following [this](https://github.com/kowainik/org/blob/main/style-guide.md).
- line length should not exceed 80 chars.
- Every exposable api needs to have haddock documentation as per [this](https://kowainik.github.io/posts/haddock-tips).
- Every query function must be wrapped inside a try catch block. Preferred `wrapQueryFunc` function, which
  returns an Either error string or (). Using Either, we can throw error if we receive a Left.
- Try using `Overloaded record dot` syntax whenever possible.

## Version Control

- I will be follow [Git flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) workflow.
- All the changes will be commited to `develop` branch.
- Every 3 days, after thorough testing, changes from `develop` would be merged into `main`.


## Directory Structure

Proper directory structure needs to be followed. Will add the structure soon.
