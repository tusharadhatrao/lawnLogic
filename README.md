## LawnLogic

LawnLogic is a turf/court management system written in haskell. Using this system,
users can book certain facilities such as badminton court, football turf.

## How to run

### Clone the repo

```bash
 $ git clone https://gitlab.com/tusharadhatrao/lawnLogic
```

### Build the project

```bash
  $ cd <project-name>
  $ make build
```

### Run the project by passing the config file.

```bash
  $ make run ARGS="./appConfig.json"
```

### Note

The project is under development, please read wiki for future feature additions.