{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE DataKinds #-}
module Main where

import Test.Hspec
import Test.Hspec.Wai
import Test.Hspec.Wai.JSON
import LawnLogic.Platform.Core
import LawnLogic.Common.Types
import qualified Data.Text as T
import qualified Data.ByteString.Lazy.Char8 as BS
import Servant
import Servant.Auth.Server
import LawnLogic.Platform.API
import LawnLogic.Common.Model
import Data.Aeson
import Data.Time
import Web.LawnLogicSpec

testMain :: IO ()
testMain = do
    eConfig <- getTestConfig
    case eConfig of
        Left err -> putStrLn $ "Error: " ++ T.unpack (unWrap err)
        Right cfg -> do
            putStrLn $ "Starting test" 
            jwtSecretKey <- generateKey
            let jwtSett = defaultJWTSettings jwtSecretKey
                ctx = defaultCookieSettings :. jwtSett :. EmptyContext
            testUser <- fetchTestUser
            testAdmin <- fetchTestAdmin
            eUserToken <- makeJWT testUser jwtSett Nothing
            eAdminToken <- makeJWT testAdmin jwtSett Nothing
            case eUserToken of
                Left _ -> putStrLn "User Token Creation Failed!" >> pure ()
                Right userToken -> do
                    case eAdminToken of
                        Left _ -> putStrLn "Admin Token Creation Failed!" >> pure ()
                        Right adminToken -> do
                            hspec $ spec (app ctx jwtSett cfg) userToken adminToken
  where
    app ctx jwtSett cfg = serveWithContext (Proxy :: Proxy (MainAPI '[JWT,Cookie]))
            ctx (allServer cfg jwtSett)

main :: IO ()
main = testMain
