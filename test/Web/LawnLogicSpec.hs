{-# LANGUAGE DisambiguateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
module Web.LawnLogicSpec where

import Test.Hspec
import Test.Hspec.Wai
import LawnLogic.Common.Types
import qualified Data.Text as T
import qualified Data.ByteString.Lazy.Char8 as BSL
import Servant
import LawnLogic.Common.Model
import Data.Aeson
import Data.Time
import Data.String.Interpolate
import qualified Data.ByteString.Lazy as BS

adminLoginCorrectInput :: BS.ByteString
adminLoginCorrectInput = [i|
    {
	"adminLoginEmail": "peter@lawnlogic.com",
	"adminLoginPassword": "foobar1"
    }
|]

adminLoginIncorrectInput :: BS.ByteString
adminLoginIncorrectInput = [i|
    {
	"adminLoginEmail": "peter@lawnlogic.com",
	"adminLoginPassword": "foobar"
    }
|]

userLoginCorrectInput :: BS.ByteString
userLoginCorrectInput = [i|
   {
	"loginUserEmail" : "tushar@gmail.com",
	"loginUserPassword" : "foobar1"
    }
|]

userLoginIncorrectInput :: BS.ByteString
userLoginIncorrectInput = [i|
   {
	"loginUserEmail" : "tushar@gmil.com",
	"loginUserPassword" : "foobar1"
    }
|]

addRatingInput :: BS.ByteString
addRatingInput = [i|
    {
	"rating":7,
	"review":"good maintainance!"
    }
|]
addWaitlistInput :: BS.ByteString
addWaitlistInput = [i|
    {
	"facilityId" : 1,
	"startTime" : "2011-10-10T15:48:00.000+09:00",
	"endTime": "2011-10-10T17:48:00.000+09:00"
    }
|]

payBookingInput :: BS.ByteString
payBookingInput = [i|
  {
	"bookingId":17,
	"payingAmount":1000
}
    |]

setHolidayInput :: BS.ByteString
setHolidayInput = [i|
    {
	"facilityId" : 1,
	"startDate" : "2011-09-10T14:48:00.000+09:00",
	"endDate" : "2011-11-10T14:48:00.000+09:00",
	"typeOfHoliday" : "holiday"
}
|]

bookFacilityInput :: BS.ByteString
bookFacilityInput = [i|
    {
    "facilityId" : 1,
    "startTime" : "2011-10-10T15:48:00.000+09:00",
    "endTime": "2011-10-10T17:48:00.000+09:00"
}
|]

userRegisterInput :: BS.ByteString
userRegisterInput = [i|
    {
	"registerUserName" : "tushar2",
	"registerUserEmail" : "tushar2@gmail.com",
	"registerUserPassword" : "foobar",
	"registerUserConfirmPassword" : "foobar"
}
    |]

spec :: Application -> BS.ByteString -> BS.ByteString -> Spec
spec app userToken adminToken = with (return app) $ do
    describe "GET /admin/dashboard" $ do
        it "responds with forbidden" $ do
            get "/admin/dashboard" `shouldRespondWith` 403
    describe "GET /search_facility" $ do
        it "responds with []" $ do
            request "GET" "/search_facility" [("Content-Type","application/json")] "{}" 
                `shouldRespondWith` 200
    describe "POST /admin/login" $ do
        it "responds with 200" $ do
            request "POST" "/admin/login" [("Content-Type","application/json")] 
                adminLoginCorrectInput `shouldRespondWith` 200
    describe "POST /admin/login" $ do
        it "responds with 200" $ do
            request "POST" "/admin/login" [("Content-Type","application/json")] 
                adminLoginIncorrectInput `shouldRespondWith` 401
    describe "GET /admin/dashboard" $ do
        it "responds with forbidden" $ do
            request "GET" "/admin/dashboard" [("Authorization","Bearer " <> BS.toStrict userToken)] 
                "" `shouldRespondWith` 403
    describe "POST /user/login" $ do
        it "responds with 200" $ do
            request "POST" "/user/login" [("Content-Type","application/json")] 
                userLoginCorrectInput `shouldRespondWith` 200
    describe "POST /user/login" $ do
        it "responds with Forbidden" $ do
            request "POST" "/user/login" [("Content-Type","application/json")] 
                userLoginIncorrectInput `shouldRespondWith` 403
    describe "GET /user/profile" $ do
        it "responds with 200" $ do
            request "GET" "/user/profile" [("Authorization","Bearer " <> BS.toStrict userToken)] 
                "" `shouldRespondWith` 200
    describe "GET /user/profile" $ do
        it "responds with Forbidden" $ do
            request "GET" "/user/profile" [("Authorization","Bearer " <> BS.toStrict adminToken)] 
                "" `shouldRespondWith` 403
    -- Assuming facilityId 1 exists
    describe "POST /user/add_rating/1" $ do
        it "responds with 200" $ do
            request "POST" "/user/add_rating/1" [("Authorization","Bearer " <> BS.toStrict userToken)
                ,("Content-Type","application/json")] 
                    addRatingInput `shouldRespondWith` 200
    describe "POST /user/add_rating/1" $ do
        it "responds with Forbidden" $ do
            request "POST" "/user/add_rating/1" [("Authorization","Bearer " <> BS.toStrict adminToken)
                ,("Content-Type","application/json")] 
                    addRatingInput `shouldRespondWith` 403
    -- Assuming bookingId 16 exists for user 3
    describe "DELETE /user/cancel_booking/16" $ do
        it "responds with 200" $ do
            request "DELETE" "/user/cancel_booking/16" [("Authorization","Bearer " <> BS.toStrict userToken)] 
                "" `shouldRespondWith` 200
    -- Assuming bookingId 17 exists for user 3
    describe "POST /user/verify_booking/17" $ do
        it "responds with 200" $ do
            request "POST" "/user/verify_booking/17" [("Authorization","Bearer " <> BS.toStrict userToken)
                ,("Content-Type","application/json")] 
                    "{otp:4321}" `shouldRespondWith` 200
    -- Assuming FacilitId 1 is booked for given slot
    describe "POST /user/add_waitlist" $ do
        it "responds with 200" $ do
            request "POST" "/user/add_waitlist" [("Authorization","Bearer " <> BS.toStrict userToken)
                ,("Content-Type","application/json")] 
                    addWaitlistInput `shouldRespondWith` 200
    -- Assuming bookingId 17 exists for user 3
    describe "POST /user/pay_booking" $ do
        it "responds with 200" $ do
            request "POST" "/user/pay_booking" [("Authorization","Bearer " <> BS.toStrict userToken)
                ,("Content-Type","application/json")] 
                    payBookingInput `shouldRespondWith` 200
    -- Assuming facilityId 1 exists
    describe "POST /admin/set_holiday/facility" $ do
        it "responds with 200" $ do
            request "POST" "/admin/set_holiday/facility" [("Authorization","Bearer " <> BS.toStrict adminToken)
                ,("Content-Type","application/json")] 
                setHolidayInput `shouldRespondWith` 200
    describe "POST /user/book_facility" $ do
        it "responds with 200" $ do
            request "POST" "/user/book_facility" [("Authorization","Bearer " <> BS.toStrict userToken)
                ,("Content-Type","application/json")] 
                bookFacilityInput `shouldRespondWith` 200
    describe "POST /user/register" $ do
        it "responds with 200" $ do
            request "POST" "/user/register" [("Content-Type","application/json")] 
                userRegisterInput `shouldRespondWith` 200

getTestConfig :: IO (Either ErrorMessage Config)
getTestConfig = do
    contents <- readFile "/home/user/haskell/lawnLogic/appConfig.json"
    case eitherDecode (BSL.pack contents) of
        Left err     -> return $ Left $ ErrorMessage $ T.pack err
        Right config -> return $ Right config

fetchTestUser :: IO User
fetchTestUser = do
    currTime <- getCurrentTime
    pure $ User {
      _userId = 3
    , _userName = "tushar"
    , _userEmail = "tuhsar@gmail.com"
    , _userPassword = "$2b$10$Yd9CUm7LIac/V15LnKBEMO1s.vcj518BEk3PwDPC9kFXYckTlBLI."
    , _createdAt = currTime
    , _updatedAt = currTime
}

fetchTestAdmin :: IO Admin
fetchTestAdmin = do
    currTime <- getCurrentTime
    pure $ Admin {
      _adminId = 1
    , _adminName = "peter"
    , _adminEmail = "peter@lawnlogic.com"
    , _adminPassword = "$2a$10$IvKiQdPJ3Bb3EpgfTGx1gOV.SvqXh50yEu5LNgH4CuE3bw625zJjm"
    , _createdAt  = currTime
    , _updatedAt = currTime
    }

