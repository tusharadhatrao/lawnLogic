build: 
	cabal build
run: 
	cabal exec lawnLogic $(ARGS)
clean:
	cabal clean
testing:
	cabal test
