begin;

-- Admin table
CREATE TABLE admin (
    admin_id        SERIAL PRIMARY KEY,
    admin_name      VARCHAR(255) NOT NULL,
    admin_email     VARCHAR(255) UNIQUE NOT NULL,
    admin_password  TEXT,
    created_at      TIMESTAMPTZ,
    updated_at      TIMESTAMPTZ
);

-- Users table
CREATE TABLE users (
    user_id         SERIAL PRIMARY KEY,
    user_name       VARCHAR(255) NOT NULL,
    user_email      VARCHAR(255) UNIQUE NOT NULL,
    user_password   TEXT,
    created_at      TIMESTAMPTZ,
    updated_at      TIMESTAMPTZ
);

-- Facility Group table
CREATE TABLE facility_group (
    group_id        SERIAL PRIMARY KEY,
    group_name      VARCHAR(255) NOT NULL,
    created_at      TIMESTAMPTZ,
    updated_at      TIMESTAMPTZ
);

-- Facility table
CREATE TABLE facility (
    facility_id     SERIAL PRIMARY KEY,
    facility_name   VARCHAR(255) NOT NULL,
    facility_type   VARCHAR(50) CHECK (facility_type IN ('tennisCourt', 'footballTurf', 'badmintonCourt')),
    min_book_hour   TIME,
    facility_address VARCHAR(255) NOT NULL,
    facility_images VARCHAR(255)[],
    group_id        INT REFERENCES facility_group(group_id) on delete set NULL,
    created_at      TIMESTAMPTZ,
    updated_at      TIMESTAMPTZ
);

-- Bookings table
CREATE TABLE bookings (
    booking_id          SERIAL PRIMARY KEY,
    user_id             INT REFERENCES users(user_id),
    facility_id         INT REFERENCES facility(facility_id),
    date_time           TIMESTAMPTZ,
    book_for_how_many_hours TIME,
    booking_status      VARCHAR(50) CHECK (booking_status IN ('pending', 'confirmed', 'cancelled', 'revoked')),
    created_at          TIMESTAMPTZ,
    updated_at          TIMESTAMPTZ,
);

-- Holiday/Maintenance Days table
CREATE TABLE holiday_maintenance_days (
    facility_id     INT REFERENCES facility(facility_id),
    start_date      TIMESTAMPTZ,
    end_date        TIMESTAMPTZ,
    type_of_holiday VARCHAR(50) CHECK (type_of_holiday IN ('holiday', 'maintenance'))
);

-- Validating Tokens table
CREATE TABLE validating_tokens (
    token           INT CHECK (token BETWEEN 1000 AND 9999),
    booking_id      INT REFERENCES bookings(booking_id),
    created_at      TIMESTAMPTZ,
    updated_at      TIMESTAMPTZ
);

-- Waitlist table
CREATE TABLE waitlist (
    waitlist_id     SERIAL PRIMARY KEY,
    user_id         INT REFERENCES users(user_id),
    facility_id     INT REFERENCES facility(facility_id),
    preferred_start_date_time TIMESTAMPTZ,
    preferred_end_date_time TIMESTAMPTZ,
    how_many_hours_you_want TIME,
    created_at      TIMESTAMPTZ,
    updated_at      TIMESTAMPTZ
);

-- Booking Subscription table
CREATE TABLE booking_subscription (
    subscription_id SERIAL PRIMARY KEY,
    user_id         INT REFERENCES users(user_id),
    facility_id     INT REFERENCES facility(facility_id),
    start_datetime_of_sub TIMESTAMPTZ,
    recurring_days  DATE
);

-- Example trigger for preventing booking during maintenance/holiday
CREATE OR REPLACE FUNCTION prevent_booking_during_maintanance()
RETURNS TRIGGER AS $$
BEGIN
    IF EXISTS (
        SELECT 1
        FROM holiday_maintanance_days
        WHERE 
            facility_id = NEW.facility_id AND
            NEW.date_time BETWEEN start_date AND end_date AND
            type_of_holiday = 'maintanance'
    ) THEN
        RAISE EXCEPTION 'Cannot book during maintenance period';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


-- Admin table
ALTER TABLE admin
ALTER COLUMN created_at SET DEFAULT NOW(),
ALTER COLUMN updated_at SET DEFAULT NOW();

-- Users table
ALTER TABLE users
ALTER COLUMN created_at SET DEFAULT NOW(),
ALTER COLUMN updated_at SET DEFAULT NOW();

-- Facility Group table
ALTER TABLE facility_group
ALTER COLUMN created_at SET DEFAULT NOW(),
ALTER COLUMN updated_at SET DEFAULT NOW();

-- Facility table
ALTER TABLE facility
ALTER COLUMN created_at SET DEFAULT NOW(),
ALTER COLUMN updated_at SET DEFAULT NOW();

-- Bookings table
ALTER TABLE bookings
ALTER COLUMN created_at SET DEFAULT NOW(),
ALTER COLUMN updated_at SET DEFAULT NOW();

-- Waitlist table
ALTER TABLE waitlist
ALTER COLUMN created_at SET DEFAULT NOW(),
ALTER COLUMN updated_at SET DEFAULT NOW();

-- Booking Subscription table
ALTER TABLE booking_subscription
ALTER COLUMN start_datetime_of_sub SET DEFAULT NOW();

insert into "admin" (admin_name,admin_email,admin_password) values 
('peter','peter@lawnlogic.com','$2a$10$IvKiQdPJ3Bb3EpgfTGx1gOV.SvqXh50yEu5LNgH4CuE3bw625zJjm');

alter table "admin" alter column admin_password set not null;

alter table facility alter column min_book_hour set not null;
create type facility_types as enum ('tennisCourt','footballTurf','badmintonCourt');
alter table facility alter column facility_type type facility_types;
alter table facility alter column facility_type set not null;

alter table facility add column rent_per_min_book_hour numeric(8,2) check (rent_per_min_book_hour >= 0);
alter table facility add column facility_start_time time;
alter table facility add column facility_end_time time;

alter table facility alter column rent_per_min_book_hour set not null;
alter table facility alter column facility_start_time set not null;
alter table facility alter column facility_end_time set not null;

alter table facility alter column facility_images type text;
alter table facility alter column rent_per_min_book_hour type int;
alter table facility drop column group_id;

drop table facility_group;

CREATE TABLE fgroup (
    group_id        SERIAL PRIMARY KEY,
    group_name      VARCHAR(255) NOT NULL,
    created_at      TIMESTAMPTZ default now(),
    updated_at      TIMESTAMPTZ default now()
);

create table facility_group (
	group_id int references fgroup (group_id) on delete cascade,
	facility_id int references facility(facility_id) on delete cascade,
	primary key (group_id,facility_id)
);

alter table holiday_maintenance_days rename to holiday_maintainace_days;
alter table bookings drop column book_for_how_many_hours;
alter table bookings add column end_time timestamptz;
alter table bookings rename column date_time to start_time;
alter table holiday_maintainace_days rename to holiday_maintainance_days; 
alter table bookings add column otp varchar(4);
alter table bookings add column is_verified bool;
alter table bookings alter column is_verified set default false;
alter table waitlist drop column how_many_hours_you_want;

alter table booking_subscription drop column recurring_days;
alter table booking_subscription add column end_date_time timestamptz not null;
alter table booking_subscription add column interval_days int not null;
alter table booking_subscription add column created_at timestamptz default now();
alter table booking_subscription add column time_slot timestamptz not null;

CREATE TABLE ratings (
    rating_id SERIAL PRIMARY KEY,
    user_id int references users(user_id) on delete cascade NOT NULL,
    facility_id int references facility(facility_id) on delete cascade NOT NULL,
    rating INTEGER CHECK (rating >= 0 AND rating <= 10) NOT NULL,
    review TEXT,
    created_at TIMESTAMP DEFAULT now(),
    updated_at TIMESTAMP DEFAULT now(),
    UNIQUE (user_id, facility_id)
);

CREATE INDEX idx_user_facility ON ratings (user_id, facility_id);

CREATE OR REPLACE FUNCTION update_updated_at_column()
RETURNS TRIGGER AS $$
BEGIN
  NEW."updated_at" = NOW();
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;


create or replace TRIGGER set_updated_at_facility
BEFORE UPDATE ON facility
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

create or replace TRIGGER set_updated_at_admin
BEFORE UPDATE ON admin
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

create or replace TRIGGER set_updated_at_bookings
BEFORE UPDATE ON bookings
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

create or replace TRIGGER set_updated_at_fgroup
BEFORE UPDATE ON fgroup
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

create or replace TRIGGER set_updated_at_users
BEFORE UPDATE ON users
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

create or replace TRIGGER set_updated_at_validating_tokens
BEFORE UPDATE ON validating_tokens
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

create or replace TRIGGER set_updated_at_waitlist
BEFORE UPDATE ON waitlist
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

create or replace TRIGGER set_updated_at_ratings
BEFORE UPDATE ON ratings
FOR EACH ROW
EXECUTE FUNCTION update_updated_at_column();

commit;
