{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
module LawnLogic.Common.Model where

import Database.Beam
import GHC.Int
import Data.Text (Text)
import Data.Time
import Servant.Auth.Server
import Data.Aeson
import Database.PostgreSQL.Simple.Time

data UserT f = User {
    _userId :: Columnar f Int32
  , _userName :: Columnar f Text
  , _userEmail :: Columnar f Text
  , _userPassword :: Columnar f Text
  , _createdAt   :: Columnar f UTCTime
  , _updatedAt   :: Columnar f UTCTime
} deriving (Generic,Beamable)

data AdminT f = Admin {
    _adminId         :: Columnar f Int32
  , _adminName       :: Columnar f Text
  , _adminEmail      :: Columnar f Text
  , _adminPassword   :: Columnar f Text
  , _createdAt       :: Columnar f UTCTime
  , _updatedAt       :: Columnar f UTCTime
} deriving (Generic, Beamable)

data FacilityT f = Facility {
    _facilityId             :: Columnar f Int32
  , _facilityName           :: Columnar f Text
  , _facilityType           :: Columnar f Text
  , _minBookHour            :: Columnar f TimeOfDay
  , _facilityAddress        :: Columnar f Text
  , _facilityImage          :: Columnar f (Maybe FilePath)
  , _createdAt              :: Columnar f UTCTime
  , _updatedAt              :: Columnar f UTCTime
  , _rentPerMinBookHour     :: Columnar f Double
  , _facilityStartTime      :: Columnar f TimeOfDay
  , _facilityEndTime        :: Columnar f TimeOfDay
} deriving (Generic, Beamable)

-- FGroup table is just naming convetion, since group is a keyword
-- in postgreSQL. Since postgres has the table named `fGroup` I
-- am also naming it Fgroup.
data FGroupT f = FGroup {
      _groupId :: Columnar f Int32
    , _groupName :: Columnar f Text
    , _createdAt  :: Columnar f UTCTime
    , _updatedAt  :: Columnar f UTCTime
} deriving (Generic,Beamable)

data FacilityGroupT f = FacilityGroup {
      _groupIdForFacilityGroup :: PrimaryKey FGroupT f
    , _facilityIdForFacilityGroup :: PrimaryKey FacilityT f
} deriving (Generic,Beamable)

data BookingT f = Booking {
      _bookingId             :: Columnar f Int32
    , _userIdForBooking      :: PrimaryKey UserT f
    , _facilityIdForBooking  :: PrimaryKey FacilityT f
    , _startTime             :: Columnar f UTCTime
    , _bookingStatus         :: Columnar f Text
    , _createdAt             :: Columnar f UTCTime
    , _updatedAt             :: Columnar f UTCTime
    , _endTime               :: Columnar f UTCTime
    , _otp                   :: Columnar f (Maybe Text)
    , _isVerified            :: Columnar f Bool
} deriving (Generic,Beamable)

data HolidayMaintainaceDaysT f = HolidayMaintainaceDays {
      _facilityIdForMaintainance :: PrimaryKey FacilityT f
    , _startDate  :: Columnar f UTCTime
    , _endDate    :: Columnar f UTCTime
    , _typeOfHoliday :: Columnar f Text
} deriving (Generic,Beamable)

data WaitlistT f = Waitlist {
      _waitlistId            :: Columnar f Int32
    , _userIdForWaitlist     :: PrimaryKey UserT f
    , _facilityIdForWaitlist :: PrimaryKey FacilityT f
    , _preferredStartTime    :: Columnar f UTCTime
    , _preferredEndTime      :: Columnar f UTCTime
    , _createdAt             :: Columnar f UTCTime
    , _updatedAt             :: Columnar f UTCTime
} deriving (Generic,Beamable)

data RatingT f = Rating {
      _ratingId            :: Columnar f Int32
    , _userIdForRating     :: PrimaryKey UserT f
    , _facilityIdForRating :: PrimaryKey FacilityT f
    , _ratingNum              :: Columnar f Double
    , _review              :: Columnar f (Maybe Text)
    , _createdAt           :: Columnar f UTCTime
    , _updatedAt           :: Columnar f UTCTime
} deriving (Generic,Beamable)

data BookingSubscriptionT f = BookingSubscription {
    _subscriptionId                   :: Columnar f Int32
  , _userIdForBookingSubscription     :: PrimaryKey UserT f
  , _facilityIdForBookingSubscription :: PrimaryKey FacilityT f
  , _startDate                        :: Columnar f Date
  , _endDate                          :: Columnar f Date
  , _startTime                        :: Columnar f TimeOfDay
  , _endTime                          :: Columnar f TimeOfDay
  , _intervalDays                     :: Columnar f Integer   -- Max days interval would be 32767
  , _createdAt                        :: Columnar f UTCTime
} deriving (Generic,Beamable)

type User = UserT Identity
type UserId = PrimaryKey UserT Identity
type Admin = AdminT Identity
type AdminId = PrimaryKey AdminT Identity
type Facility = FacilityT Identity
type FacilityId = PrimaryKey FacilityT Identity
type FGroup = FGroupT Identity
type FGroupId = PrimaryKey FGroupT Identity
type FacilityGroup = FacilityGroupT Identity
type FacilityGroupId = PrimaryKey FacilityGroupT Identity
type Booking = BookingT Identity
type BookingId              = PrimaryKey BookingT Identity
type HolidayMaintainaceDays = HolidayMaintainaceDaysT Identity
type Waitlist               = WaitlistT Identity
type WaitlistId             = PrimaryKey WaitlistT Identity
type Rating                 = RatingT Identity
type RatingId               = PrimaryKey RatingT Identity
type BookingSubscription    = BookingSubscriptionT Identity
type SubscriptionId         = PrimaryKey BookingSubscriptionT Identity


deriving instance Eq User
deriving instance Show User
deriving instance ToJSON User
deriving instance FromJSON User
deriving instance ToJWT User
deriving instance FromJWT User
deriving instance Show Admin
deriving instance Eq Admin
deriving instance ToJSON Admin
deriving instance FromJSON Admin
deriving instance ToJWT Admin
deriving instance FromJWT Admin
deriving instance Show Facility
deriving instance Eq Facility
deriving instance Show FacilityId
deriving instance Eq FacilityId
deriving instance ToJSON Facility
deriving instance FromJSON Facility
deriving instance Show FGroup
deriving instance Eq FGroup
deriving instance FromJSON FGroup
deriving instance Show FGroupId
deriving instance Eq FGroupId
deriving instance Show FacilityGroup
deriving instance Eq FacilityGroup
deriving instance Eq UserId
deriving instance Show UserId
deriving instance ToJSON FacilityId
deriving instance ToJSON UserId
deriving instance Eq Booking
deriving instance Show Booking
deriving instance ToJSON Booking
deriving instance Eq HolidayMaintainaceDays
deriving instance Show HolidayMaintainaceDays
deriving instance Eq Waitlist
deriving instance Show Waitlist
deriving instance Eq WaitlistId
deriving instance Show WaitlistId
deriving instance Eq RatingId
deriving instance Show RatingId
deriving instance Eq Rating
deriving instance Show Rating
deriving instance Show BookingSubscription
deriving instance Eq BookingSubscription
deriving instance Show SubscriptionId
deriving instance Eq SubscriptionId


instance Table UserT where
    data PrimaryKey UserT f = UserId (Columnar f Int32)
        deriving (Generic,Beamable)
    primaryKey = UserId. _userId

instance Table AdminT where
    data PrimaryKey AdminT f = AdminId (Columnar f Int32) 
        deriving (Generic, Beamable)
    primaryKey = AdminId . _adminId

instance Table FacilityT where
    data PrimaryKey FacilityT f = FacilityId (Columnar f Int32)
        deriving (Generic, Beamable)
    primaryKey = FacilityId . _facilityId

instance Table FGroupT where
    data PrimaryKey FGroupT f = FGroupId (Columnar f Int32)
        deriving (Generic,Beamable)
    primaryKey = FGroupId . _groupId

instance Table FacilityGroupT where
    data PrimaryKey FacilityGroupT f = FacilityGroupId (PrimaryKey FGroupT f) (PrimaryKey FacilityT f)
        deriving (Generic,Beamable)
    primaryKey = FacilityGroupId <$> _groupIdForFacilityGroup <*> _facilityIdForFacilityGroup

instance Table BookingT where
    data PrimaryKey BookingT f = BookingId (Columnar f Int32)
        deriving (Generic,Beamable)
    primaryKey = BookingId . _bookingId

instance Table HolidayMaintainaceDaysT where
    data PrimaryKey HolidayMaintainaceDaysT f = BadNoId
        deriving (Generic,Beamable)
    primaryKey _ = BadNoId

instance Table WaitlistT where
    data PrimaryKey WaitlistT f = WaitlistId (Columnar f Int32)
        deriving (Generic,Beamable)
    primaryKey = WaitlistId . _waitlistId

instance Table RatingT where
    data PrimaryKey RatingT f = RatingId (Columnar f Int32)
        deriving (Generic,Beamable)
    primaryKey = RatingId . _ratingId

instance Table BookingSubscriptionT where
    data PrimaryKey BookingSubscriptionT f = SubscriptionId (Columnar f Int32)
        deriving (Generic,Beamable)
    primaryKey = SubscriptionId . _subscriptionId

-- Database strucuture
data Db f = Db { 
       _admin                  :: f (TableEntity AdminT)
     , _facility               :: f (TableEntity FacilityT)
     , _group                  :: f (TableEntity FGroupT)
     , _facilityGroup          :: f (TableEntity FacilityGroupT)
     , _user                   :: f (TableEntity UserT)
     , _booking                :: f (TableEntity BookingT)
     , _holidayMaintainaceDays :: f (TableEntity HolidayMaintainaceDaysT)
     , _waitlist               :: f (TableEntity WaitlistT)
     , _rating                 :: f (TableEntity RatingT)
     , _bookingSubscription    :: f (TableEntity BookingSubscriptionT)
    } deriving (Generic, Database be)

-- the table naming convention is unpredicatable.
-- So, I am specifying the table names and fields explicitly.
db :: DatabaseSettings be Db
db = defaultDbSettings `withDbModification` dbModification {
    _admin = setEntityName "admin" <> modifyTableFields tableModification {
          _adminId = "admin_id"
        , _adminName = "admin_name"
        , _adminEmail = "admin_email"
        , _adminPassword = "admin_password"
        , _createdAt = "created_at"
        , _updatedAt = "updated_at"
    }
    , _facility = setEntityName "facility" <> modifyTableFields tableModification {
          _facilityId = "facility_id"
        , _facilityName = "facility_name"
        , _facilityType = "facility_type"
        , _minBookHour = "min_book_hour"
        , _facilityAddress = "facility_address"
        , _facilityImage = "facility_images"
        , _createdAt = "created_at"
        , _updatedAt = "updated_at"
        , _rentPerMinBookHour = "rent_per_min_book_hour"
        , _facilityStartTime = "facility_start_time"
        , _facilityEndTime = "facility_end_time"
    }
    , _group = setEntityName "fgroup" <> modifyTableFields tableModification {
          _groupId = "group_id"
        , _groupName = "group_name"
        , _createdAt = "created_at"
        , _updatedAt = "updated_at"
    }
    , _facilityGroup = setEntityName "facility_group" <> modifyTableFields tableModification {
          _groupIdForFacilityGroup = FGroupId "group_id"
        , _facilityIdForFacilityGroup = FacilityId "facility_id"
    }
    , _user = setEntityName "users" <> modifyTableFields tableModification {
          _userId = "user_id"
        , _userName = "user_name"
        , _userEmail = "user_email"
        , _userPassword = "user_password"
        , _createdAt  = "created_at"
        , _updatedAt = "updated_at"
    }
    ,_booking = setEntityName "bookings" <> modifyTableFields tableModification {
          _bookingId = "booking_id"
        , _userIdForBooking = UserId "user_id"
        , _facilityIdForBooking = FacilityId "facility_id"
        , _startTime = "start_time"
        , _bookingStatus = "booking_status"
        , _createdAt = "created_at"
        , _updatedAt = "updated_at"
        , _endTime   = "end_time"
        , _otp      = "otp"
        , _isVerified = "is_verified"
    }
    ,_holidayMaintainaceDays = setEntityName "holiday_maintainance_days" <> modifyTableFields tableModification {
          _facilityIdForMaintainance = FacilityId "facility_id"
        , _startDate = "start_date"
        , _endDate = "end_date"
        , _typeOfHoliday = "type_of_holiday"
    }
    ,_waitlist = setEntityName "waitlist" <> modifyTableFields tableModification {
          _waitlistId            = "waitlist_id"
        , _userIdForWaitlist     = UserId "user_id"
        , _facilityIdForWaitlist = FacilityId "facility_id"
        , _preferredStartTime    = "preferred_start_date_time"
        , _preferredEndTime      = "preferred_end_date_time"
        , _createdAt             = "created_at"
        , _updatedAt             = "updated_at"
    }
    ,_rating = setEntityName "ratings" <> modifyTableFields tableModification {
          _ratingId   = "rating_id"
        , _userIdForRating = UserId "user_id"
        , _facilityIdForRating = FacilityId "facility_id"
        , _ratingNum = "rating"
        , _review = "review"
        , _createdAt = "created_at"
        , _updatedAt = "updated_at"
    }
    ,_bookingSubscription = setEntityName "booking_subscription" <> modifyTableFields tableModification {
        _subscriptionId                   = "subscription_id"
      , _userIdForBookingSubscription     = UserId "user_id"
      , _facilityIdForBookingSubscription = FacilityId "facility_id"
      , _startDate                        = "start_date"
      , _endDate                          = "end_date"
      , _startTime                        = "start_time"
      , _endTime                          = "end_time"
      , _intervalDays                     = "interval_days"
      , _createdAt                        = "created_at"
    }
}
