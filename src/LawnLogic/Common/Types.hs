{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE GeneralizedNewtypeDeriving, DerivingStrategies #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DuplicateRecordFields #-}
module LawnLogic.Common.Types where

import GHC.Generics
import Data.Aeson
import Data.Text (Text)
import Control.Monad.Reader
import Control.Monad.Except
import Data.Time
import Servant
import Servant.Multipart
import GHC.Int
import Database.PostgreSQL.Simple
import Data.Scientific
import Database.PostgreSQL.Simple.FromRow (FromRow(fromRow), field)
import Data.Csv
import Database.PostgreSQL.Simple.Time

newtype ErrorMessage = ErrorMessage {unWrap :: Text}
    deriving (Eq,Show)

type TheFileType = Text
type TheFileName = Text

data DBConfig = DBConfig {
    host :: Text,
    port :: Int,
    user :: Text,
    password :: Text,
    database :: Text
} deriving (Generic, Show,FromJSON)

data FacilityType = TennisCourt | FootballTurf | BadmintonCourt
    deriving (Eq,Show,Generic,FromJSON)

data Config = Config {
      appPort :: Int
    , dbConfig :: DBConfig
    , fileUploadDir :: FilePath
} deriving (Generic, Show,FromJSON)

newtype AppT m a = AppT
  { getApp :: ReaderT Config (ExceptT ServerError m) a } 
    deriving newtype ( Functor
             , Applicative
             , Monad
             , MonadReader Config
             , MonadError ServerError
             , MonadIO
             )

data AdminLoginData = AdminLoginData {
      adminLoginEmail    :: Text
    , adminLoginPassword :: Text
} deriving (Eq,Show,Generic,FromJSON)

data UserRegisterData = UserRegisterData {
      registerUserName :: Text
    , registerUserEmail :: Text
    , registerUserPassword :: Text
    , registerUserConfirmPassword :: Text
} deriving (Eq,Show,Generic,FromJSON)

data UserLoginData = UserLoginData {
      loginUserEmail :: Text
    , loginUserPassword :: Text
} deriving (Eq,Show,Generic,FromJSON)

data BookFacilityData = BookFacilityData {
      facilityId :: Int32
    , startTime :: UTCTime
    , endTime :: UTCTime
} deriving (Eq,Show,Generic,FromJSON)

data BookingInitiatedInfo = BookingInitiatedInfo {
      message :: Text
    , bookingId :: Int32
    , paymentAmount :: Double
} deriving (Eq,Show,Generic,ToJSON)

data PayBookingData = PayBookingData {
    bookingId :: Int32,
    payingAmount :: Double
} deriving (Eq,Show,Generic,FromJSON)

data VerifyBookingData = VerifyBookingData {
    otp :: Int
} deriving (Eq,Show,Generic,FromJSON)

data AddRatingData = AddRatingData {
     rating :: Double
   , review :: Maybe Text
} deriving (Eq,Show,Generic,FromJSON)

data TopRatingResult = TopRatingResult {
    facilityName :: Text,
    rating :: Maybe Scientific
} deriving (Eq,Show,Generic,ToJSON,FromRow)

data SearchFacilityData = SearchFacilityData {
      searchTerm    :: Maybe Text
    , facilityType  :: Maybe FacilityType
    , timeRange     :: Maybe [UTCTime]
} deriving (Eq,Show,Generic,FromJSON)

data FetchAvailableSlotData = FetchAvailableSlotData {
      facilityId :: Int32
    , date :: Day
} deriving (Eq,Show,Generic,FromJSON,FromRow)

data BookingStatistics = BookingStatistics {
      mostBookedFacility    :: MostBookedFacility
    , mostCancelledFacility :: MostCancelledFacility
    , mostUnusedFacility    :: MostUnusedFacility
    , peakBookingHours      :: Maybe [PeekBookingHours]
} deriving (Eq,Show,Generic,ToJSON)

instance FromRow Text where
    fromRow = field

instance FromRow PeekBookingHours where
    fromRow = PeekBookingHours <$> field

instance ToNamedRecord BookingStatistics
instance DefaultOrdered BookingStatistics

instance ToField [PeekBookingHours] where
    toField lst = toField $ show lst

instance ToField MostCancelledFacility where
    toField (MostCancelledFacility Nothing) = toField ("NULL" :: String)
    toField (MostCancelledFacility (Just x)) = toField x

instance ToField MostBookedFacility where
    toField (MostBookedFacility Nothing) = toField ("NULL" :: String)
    toField (MostBookedFacility (Just x)) = toField x

instance ToField MostUnusedFacility where
    toField (MostUnusedFacility Nothing) = toField ("NULL" :: String)
    toField (MostUnusedFacility (Just x)) = toField x

newtype PeekBookingHours = PeekBookingHours UTCTime
    deriving newtype (Eq,Show,ToJSON)

newtype MostCancelledFacility = MostCancelledFacility (Maybe Text)
    deriving newtype (Eq,Show,ToJSON)

newtype MostBookedFacility = MostBookedFacility (Maybe Text)
    deriving newtype (Eq,Show,ToJSON)

newtype MostUnusedFacility = MostUnusedFacility (Maybe Text)
    deriving newtype (Eq,Show,ToJSON)

data AvailableSlotData = AvailableSlotData {
      facilityId            :: Int32
    , minBookHour           :: TimeOfDay
    , facilityStartTime     :: TimeOfDay
    , facilityEndTime       :: TimeOfDay
    , bookingStartTime      :: Maybe UTCTime
    , bookingEndTime        :: Maybe UTCTime
    , maintainanceStartTime :: Maybe UTCTime
    , maintainanceEndTime   :: Maybe UTCTime
} deriving (Eq,Show,Generic,ToJSON,FromRow)

data AvailableSlotFacility = AvailableSlotFacility {
      minBookHour           :: TimeOfDay
    , facilityStartTime     :: TimeOfDay
    , facilityEndTime       :: TimeOfDay
} deriving (Eq,Show,Generic,ToJSON)

data AvailableSlotResult = AvailableSlotResult{
      bookingStartTime      :: Maybe UTCTime
    , bookingEndTime        :: Maybe UTCTime
    , maintainanceStartTime :: Maybe UTCTime
    , maintainanceEndTime   :: Maybe UTCTime
} deriving (Eq,Show,Generic,ToJSON)

data AvailableSlotResult_ = AvailableSlotResult_ {
      availableSlotFacility :: AvailableSlotFacility
    , availableSlotResult :: [AvailableSlotResult]
} deriving (Eq,Show,Generic,ToJSON)

data FacilityMultipartData = FacilityMultipartData {
     facilityName       :: Text
   , factilityType      :: Text
   , minBookHour        :: Text
   , facilityAddress    :: Text
   , rentPerMinHour     :: Text
   , facilityStartTime  :: Text
   , facilityEndTime    :: Text
   , facilityImage      :: Maybe (FilePath,TheFileType,TheFileName)
} deriving (Eq,Show,Generic,FromJSON)

data FacilityData = FacilityData {
     facilityName       :: Text
   , facilityType       :: FacilityType
   , minBookHour        :: TimeOfDay
   , facilityAddress    :: Text
   , rentPerMinHour     :: Double
   , facilityStartTime  :: TimeOfDay
   , facilityEndTime    :: TimeOfDay
   , facilityImage      :: Maybe FilePath
} deriving (Eq,Show,Generic)

data GroupData = GroupData {
      groupName :: Text
    , facilityIds :: [Int32]
} deriving (Eq,Show,Generic,FromJSON)

data UpdateGroupData = UpdateGroupData {
     facilityType       :: Maybe FacilityType
   , minBookHour        :: Maybe TimeOfDay
   , facilityAddress    :: Maybe Text
   , rentPerMinHour     :: Maybe Double
   , facilityStartTime  :: Maybe TimeOfDay
   , facilityEndTime    :: Maybe TimeOfDay
   , facilityImage      :: Maybe FilePath
} deriving (Eq,Show,Generic)

data UpdateGroupMultipartData = UpdateGroupMultipartData {
     facilityType       :: Maybe Text
   , minBookHour        :: Maybe Text
   , facilityAddress    :: Maybe Text
   , rentPerMinHour     :: Maybe Text
   , facilityStartTime  :: Maybe Text
   , facilityEndTime    :: Maybe Text
   , facilityImage      :: Maybe (FilePath,TheFileType,TheFileName)
} deriving (Eq,Show,Generic,FromJSON)

data SetFacilityHolidayData = SetFacilityHolidayData {
      facilityId :: Int32
    , startDate :: UTCTime
    , endDate   :: UTCTime
    , typeOfHoliday :: Text
} deriving (Eq,Show,Generic,FromJSON)

data AddSubscriptionData = AddSubscriptionData {
      startDate     :: Day
    , endDate       :: Day
    , startTime     :: TimeOfDay
    , endTime       :: TimeOfDay
    , intervalDays  :: Integer
} deriving (Eq,Show,Generic,FromJSON)

instance FromJSON Date where
    parseJSON d = Finite <$> parseJSON d

instance FromMultipart Tmp FacilityMultipartData where
    fromMultipart multipartData =
        FacilityMultipartData
                        <$> lookupInput "facilityName" multipartData
                        <*> lookupInput "facilityType" multipartData
                        <*> lookupInput "minBookHour" multipartData
                        <*> lookupInput "facilityAddress" multipartData
                        <*> lookupInput "rentPerMinHour" multipartData
                        <*> lookupInput "facilityStartTime" multipartData
                        <*> lookupInput "facilityEndTime" multipartData
                        <*> getFileInfoIfExist (lookupFile "facilityImage" multipartData)
        where
            getFileInfoIfExist :: Either String (FileData Tmp) 
                               -> Either String (Maybe (FilePath,TheFileType,TheFileName))
            getFileInfoIfExist (Left _) = Right Nothing
            getFileInfoIfExist (Right fData) = Right $ Just (fdPayload fData,fdFileCType fData,fdFileName fData)

instance FromMultipart Tmp UpdateGroupMultipartData where
    fromMultipart multipartData = UpdateGroupMultipartData
                        <$> getInfoIfExist (lookupInput "facilityType" multipartData)
                        <*> getInfoIfExist (lookupInput "minBookHour" multipartData)
                        <*> getInfoIfExist (lookupInput "facilityAddress" multipartData)
                        <*> getInfoIfExist (lookupInput "rentPerMinHour" multipartData)
                        <*> getInfoIfExist (lookupInput "facilityStartTime" multipartData)
                        <*> getInfoIfExist (lookupInput "facilityEndTime" multipartData)
                        <*> getFileInfoIfExist (lookupFile "facilityImage" multipartData)
        where
            getInfoIfExist :: Either String a -> Either String (Maybe a)
            getInfoIfExist (Left _) = Right Nothing
            getInfoIfExist (Right a) = Right $ Just a

            getFileInfoIfExist :: Either String (FileData Tmp) 
                               -> Either String (Maybe (FilePath,TheFileType,TheFileName))
            getFileInfoIfExist (Left _) = Right Nothing
            getFileInfoIfExist (Right fData) = Right $ Just (fdPayload fData,fdFileCType fData,fdFileName fData)
