{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
module LawnLogic.Common.Utils where

import           System.Environment
import           Data.Aeson
import           LawnLogic.Common.Types
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.Text as T
import           Database.PostgreSQL.Simple
import           Data.Text (Text)
import           Control.Exception
import           Control.Monad.Reader (MonadIO)
import           Servant (errBody,err400,throwError)
import           Data.Time
import           Database.PostgreSQL.Simple.Time

getConn :: Config -> ConnectInfo
getConn Config{dbConfig=DBConfig{..}} = do
    defaultConnectInfo {
        connectHost = T.unpack host,
        connectUser = T.unpack user,
        connectPassword = T.unpack password,
        connectDatabase = T.unpack database
    }

getConfig :: IO (Either ErrorMessage Config)
getConfig = do
    args <- getArgs
    case args of
        [configFile] -> do
            contents <- readFile configFile
            case eitherDecode (BS.pack contents) of
                Left err     -> return $ Left $ ErrorMessage $ T.pack err
                Right config -> return $ Right config
        _ -> return $ Left $ ErrorMessage "Usage: LawnLogic <config file>"

facilityTypeToText :: FacilityType -> Text
facilityTypeToText FootballTurf = "footballTurf"
facilityTypeToText TennisCourt = "footballTurf"
facilityTypeToText BadmintonCourt = "footballTurf"

wrapQueryFunc :: (IO a) -> IO (Either SomeException a)
wrapQueryFunc queryFunc = catch (Right <$> queryFunc)
                            (\(e :: SomeException) -> pure $ Left e)

fetchOrThrow :: MonadIO m => AppT m (Either SomeException a) -> AppT m a
fetchOrThrow func = do
    eRes <- func
    case eRes of
        Left e -> throwError $ err400 {errBody = BS.pack $ show e}
        Right x -> pure x

dayToDate :: Day -> Date
dayToDate d = Finite d
