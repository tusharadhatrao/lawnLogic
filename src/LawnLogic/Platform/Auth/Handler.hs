{-# LANGUAGE DataKinds       #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module LawnLogic.Platform.Auth.Handler where

import           Servant
import           Servant.Auth.Server
import           LawnLogic.Common.Types
import           LawnLogic.Common.Model
import           Data.Password.Bcrypt
import           Control.Monad.IO.Class
import           Data.Text (Text)
import           Data.Text.Lazy.Encoding (decodeUtf8)
import           Control.Monad.Reader
import           LawnLogic.Platform.DB.Query
import qualified Data.Text.Lazy as T
import           Control.Exception
import qualified Data.ByteString.Lazy.Char8 as BSC

userLoginH :: MonadIO m 
           => CookieSettings 
           -> JWTSettings 
           -> UserLoginData 
           -> AppT m (Headers '[Header "Set-Cookie" SetCookie
                                    , Header "Set-Cookie" SetCookie] Text)
userLoginH cookieSett jwtSett UserLoginData{..} = do
    cfg <- ask
    mUser <- liftIO $ fetchUserQ (dbConfig cfg) loginUserEmail
    case mUser of
        Nothing -> throwError $ err401 {errBody = "email/password is wrong!"}
        Just user -> do
            case (checkPassword (mkPassword loginUserPassword )(PasswordHash (fetchUserPassword user))) of
                PasswordCheckFail -> throwError $ err401 {errBody = "email/password is wrong!"}
                PasswordCheckSuccess -> do
                    mLoginAccepted <- liftIO $ acceptLogin cookieSett jwtSett user 
                    case mLoginAccepted of
                        Nothing -> throwError err401
                        Just x -> do
                            etoken <- liftIO $ makeJWT user jwtSett Nothing
                            case etoken of
                                Left _ -> throwError err401 {errBody = "JWT token creation failed"} 
                                Right v -> return $ x (T.toStrict $ decodeUtf8 v)
    where
        fetchUserPassword :: User -> Text
        fetchUserPassword User{..} = _userPassword
                    
userRegisterH :: MonadIO m => UserRegisterData -> AppT m ()
userRegisterH userRegisterData@UserRegisterData{..} = do
    cfg <- ask
    if (registerUserPassword == registerUserConfirmPassword) then do
        eRes <- liftIO $ catch (do 
                    (addUserQ (dbConfig cfg) userRegisterData)
                    pure $ Right ())
                    (\(e :: SomeException) -> pure $ Left e)
        case eRes of
            Left e -> throwError $ err406 {errBody = BSC.pack $ show e}
            Right _ -> pure ()
    else
        throwError $ err406 {errBody = "Password and confirm password not matched!"}

adminLoginH :: MonadIO m => CookieSettings -> JWTSettings -> AdminLoginData -> AppT m
    (Headers '[Header "Set-Cookie" SetCookie, Header "Set-Cookie" SetCookie]
       Text)
adminLoginH cookieSett jwtSett AdminLoginData{..} = do
    cfg <- ask
    mAdmin' <- liftIO $ getAdminByEmailQ (dbConfig cfg) adminLoginEmail
    case mAdmin' of
        -- Admin with given email, not found in DB
        Nothing ->  throwError err401 {errBody = "email/password is wrong"}
        Just admin@Admin{..} -> do
            if isValid adminLoginPassword _adminPassword then do
                mLoginAccepted <- liftIO $ acceptLogin cookieSett jwtSett admin
                case mLoginAccepted of
                    Nothing -> throwError err401
                    Just x -> do
                        etoken <- liftIO $ makeJWT admin jwtSett Nothing
                        case etoken of
                            Left _ -> throwError err401 {errBody = "JWT token creation failed"} 
                            Right v -> return $ x (T.toStrict $ decodeUtf8 v)
            else throwError err401 {errBody = "email/password is wrong"}
  where
    isValid adminLoginPassword' adminPassword = 
        case checkPassword 
            (mkPassword adminLoginPassword') 
            (PasswordHash adminPassword) of
                PasswordCheckSuccess -> True
                _                    -> False
