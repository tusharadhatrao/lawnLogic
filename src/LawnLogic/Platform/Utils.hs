{-# LANGUAGE RecordWildCards #-}
module LawnLogic.Platform.Utils where

import           LawnLogic.Common.Model
import           LawnLogic.Common.Types
import           GHC.Int
import           Data.Time
import           Data.Fixed

fetchFacilityIdNum :: FacilityId -> Int32
fetchFacilityIdNum (FacilityId x) = x

fetchUserId :: UserId -> Int32
fetchUserId (UserId x) = x

plusTenMinutes :: UTCTime -> UTCTime
plusTenMinutes = addUTCTime (secondsToNominalDiffTime 600)

fetchBookingId :: PayBookingData -> Int32
fetchBookingId PayBookingData{..} = bookingId

fetchFacilityId :: Booking -> FacilityId
fetchFacilityId Booking{..} = _facilityIdForBooking

isTimeSlotAligned :: TimeOfDay -> UTCTime -> UTCTime -> Bool
isTimeSlotAligned minBookHour startTime endTime = do
    let slot = diffUTCTime endTime startTime
        minSlot = daysAndTimeOfDayToTime 0 minBookHour
    (minSlot <= slot) && (slot `mod'` minSlot == 0)

calculateAmount :: Double -> TimeOfDay -> UTCTime -> UTCTime -> Double
calculateAmount perHourAmt minHourBook startTime endTime = perHourAmt *
    (fromRational (toRational (diffUTCTime endTime startTime)) /
    (fromIntegral (diffTimeToPicoseconds (timeOfDayToTime minHourBook)) / 1000000000000))

