{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric,DeriveAnyClass #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedRecordDot #-}
module LawnLogic.Platform.DB.Query where

import           LawnLogic.Common.Types
import           LawnLogic.Common.Model
import           Database.PostgreSQL.Simple
import           Data.Text (Text)
import qualified Data.Text as T
import           Database.Beam
import           Database.Beam.Postgres
import           LawnLogic.Common.Utils
import           Database.Beam.Backend.SQL.BeamExtensions as BeamExtensions
import           GHC.Int
import           Data.Maybe (fromMaybe)
import           Data.Password.Bcrypt
import           Data.Time
import           Data.String.Interpolate
import           Database.PostgreSQL.Simple.Types

toConnectInfo :: DBConfig -> ConnectInfo
toConnectInfo DBConfig{..} = ConnectInfo {
    connectHost = T.unpack host,
    connectPort = fromIntegral port,
    connectUser = T.unpack user,
    connectPassword = T.unpack password,
    connectDatabase = T.unpack database
}

data Booking_ = Booking_ {
      bookingId :: Int32
    , userIdForBooking    :: Int32
    , facilityIdForBooking  :: Int32
    , startTime            :: UTCTime
    , bookingStatus       :: Text
    , createdAt           :: UTCTime
    , updatedAt           :: UTCTime
    , endTime             :: UTCTime
    , otp                 :: Maybe Text
    , isVerified          :: Bool
} deriving (Generic,FromRow)

addUnconfirmedBookingQ :: DBConfig -> Int32 -> BookFacilityData -> IO (Maybe Int32)
addUnconfirmedBookingQ dbCfg userId BookFacilityData{..} = do
    bookingRes <- withConnect (toConnectInfo dbCfg) $ \conn -> do
                    runBeamPostgres conn $ runInsertReturningList $ insert (_booking db) $
                        insertExpressions [
                        Booking
                            default_
                            (val_ (UserId userId))
                            (val_ (FacilityId facilityId))
                            (val_ startTime)
                            (val_ "pending")
                            default_
                            default_
                            (val_ endTime)
                            default_
                            default_
                            ]
    case bookingRes of
        [] -> pure Nothing
        (booking:_) -> pure $ Just (_bookingId booking)


facilityAvailableQuery :: Query
facilityAvailableQuery = [i|
 select b.* 
    FROM bookings b left join holiday_maintainance_days hmd on b.facility_id  = hmd.facility_id 
  WHERE b.facility_id = ? 
        AND ((?,?) OVERLAPS (start_time, end_time) or
            (?,?) OVERLAPS (hmd.start_date ,hmd.end_date));
    |]

checkIfFacilityAvailableQ :: DBConfig -> Int32 -> UTCTime -> UTCTime -> IO [Booking_]
checkIfFacilityAvailableQ dbCfg facilityId startTimestamp endTimestamp =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        query conn facilityAvailableQuery
                (facilityId,startTimestamp,endTimestamp,startTimestamp,endTimestamp)

fetchUserQ :: DBConfig -> Text -> IO (Maybe User)
fetchUserQ dbCfg userEmail =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $  runSelectReturningOne $ select $ do
            user <- all_ (_user db)
            guard_ (_userEmail user ==. val_ userEmail)
            pure user

addUserQ :: DBConfig -> UserRegisterData -> IO ()
addUserQ dbCfg UserRegisterData{..} = do
    hashedPassword <- hashPassword $ mkPassword registerUserPassword
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runInsert $ insert (_user db) $
            insertExpressions [
                User
                    default_
                    (val_ registerUserName)
                    (val_ registerUserEmail)
                    (val_ $ unPasswordHash hashedPassword)
                    default_
                    default_
            ]

updateFacilitiesQ :: DBConfig -> [Facility] -> UpdateGroupData -> IO ()
updateFacilitiesQ dbCfg facilities UpdateGroupData{..} = do
    withConnect (toConnectInfo dbCfg) $ \conn -> do
      withTransaction conn (mapM_ (\facility0 ->
            runBeamPostgres conn $ runUpdate $
                save (_facility db) (facility0 {
                    _facilityType       = maybe (fetchFacilityType facility0)
                        facilityTypeToText facilityType
                  , _minBookHour        = fromMaybe (_minBookHour facility0) minBookHour
                  , _facilityAddress    = fromMaybe (_facilityAddress facility0) facilityAddress
                  , _rentPerMinBookHour = fromMaybe (_rentPerMinBookHour facility0) rentPerMinHour
                  , _facilityStartTime  = fromMaybe (_facilityStartTime facility0) facilityStartTime
                  , _facilityEndTime    = fromMaybe (_facilityEndTime facility0) facilityEndTime
                  , _facilityImage      = getFacilityImage (_facilityImage facility0) facilityImage
                })) facilities)
    where
        fetchFacilityType Facility{..} = _facilityType
        getFacilityImage currFacilityImage = maybe currFacilityImage Just

fetchGroupFacilitiesQ :: DBConfig -> Int32 -> IO [Facility]
fetchGroupFacilitiesQ dbCfg groupId =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runSelectReturningList $
            select $ do
                facilityGroup <- all_ (_facilityGroup db)
                guard_ (_groupIdForFacilityGroup facilityGroup ==. val_ (FGroupId groupId))
                facility <- all_ (_facility db)
                guard_ (FacilityId (_facilityId facility) ==. _facilityIdForFacilityGroup facilityGroup)
                pure facility

addFacilityQ :: DBConfig -> FacilityData -> IO ()
addFacilityQ dbCfg FacilityData{..} = do
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runInsert $ insert (_facility db) $
             insertExpressions [ Facility
                 default_ -- facilityId
                 (val_ facilityName)
                 (val_ (facilityTypeToText facilityType))
                 (val_ minBookHour)
                 (val_ facilityAddress)
                 (val_ facilityImage)
                 default_   -- createdAt
                 default_   -- updatedAt
                 (val_ rentPerMinHour)
                 (val_ facilityStartTime)
                 (val_ facilityEndTime)
             ]

updateFacilityQ :: DBConfig -> FacilityId -> FacilityData -> IO (Either String ())
updateFacilityQ dbCfg facilityId facilityData = do
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        mFacility <- runBeamPostgres conn $ runSelectReturningOne $
                        lookup_ (_facility db) facilityId
        case mFacility of
            Nothing -> pure $ Left "Facility with given id not found :("
            Just currFacility -> do
                let updatedFacility = updateFacility currFacility facilityData
                runBeamPostgres conn $ runUpdate $ save (_facility db) updatedFacility
                pure $ Right ()

updateFacility :: Facility -> FacilityData -> Facility
updateFacility currFacility FacilityData{..} = currFacility {
    _facilityName        = facilityName
   , _facilityType       = facilityTypeToText facilityType
   , _minBookHour        = minBookHour
   , _facilityAddress    = facilityAddress
   , _facilityImage     = facilityImage
   , _rentPerMinBookHour = rentPerMinHour
   , _facilityStartTime  = facilityStartTime
   , _facilityEndTime    = facilityEndTime
}

getAdminByEmailQ :: DBConfig -> Text -> IO (Maybe Admin)
getAdminByEmailQ dbCfg email = do
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runSelectReturningOne $ select $ do
            admin <- all_ (_admin db)
            guard_ (_adminEmail admin ==. val_ email)
            pure admin

insertGroupQ :: DBConfig -> Text -> IO (Maybe Int32)
insertGroupQ dbCfg groupName = do
   groupList <- withConnect (toConnectInfo dbCfg) $ \conn -> do
                  runBeamPostgres conn $ runInsertReturningList
                    $ insert (_group db) $
                      insertExpressions
                        [FGroup default_ (val_ groupName) default_ default_]
   case groupList of
     []   -> return Nothing
     (x:_)  -> return $ Just (_groupId @Identity x)

insertFacilitiesGroupQ :: DBConfig -> Int32 -> [Int32] -> IO ()
insertFacilitiesGroupQ dbCfg groupId facilityIds =
    withConnect (toConnectInfo dbCfg) $ \conn ->
        runBeamPostgres conn $ runInsert $ insert (_facilityGroup db) $
            insertValues $ map
                (FacilityGroup (FGroupId groupId) . FacilityId)
                    facilityIds

fetchFacilityGroupQ :: DBConfig -> Int32 -> Int32 -> IO (Maybe FacilityGroup)
fetchFacilityGroupQ dbCfg groupId facilityId =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runSelectReturningOne $
            lookup_ (_facilityGroup db)
                (FacilityGroupId (FGroupId groupId) (FacilityId facilityId))

deleteFacilityGroupQ :: DBConfig -> Int32 -> Int32 -> IO ()
deleteFacilityGroupQ dbCfg groupId facilityId =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runDelete $
            delete (_facilityGroup db) (\fg ->
                    (_groupIdForFacilityGroup fg) ==. val_ (FGroupId groupId)
                &&. _facilityIdForFacilityGroup fg ==. val_ (FacilityId facilityId))

addFacilityGroupQ :: DBConfig -> Int32 -> Int32 -> IO ()
addFacilityGroupQ dbCfg groupId facilityId =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runInsert $
            insert (_facilityGroup db) $
                insertValues [FacilityGroup (FGroupId groupId) (FacilityId facilityId)]

fetchGroupQ :: DBConfig -> Int32 -> IO (Maybe FGroup)
fetchGroupQ dbCfg groupId =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runSelectReturningOne $
            lookup_ (_group db) (FGroupId groupId)

deleteGroupQ :: DBConfig -> Int32 -> IO ()
deleteGroupQ dbCfg gId =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runDelete $
            delete (_group db) (\group -> _groupId group ==. val_ gId)

fetchFacilityQ :: DBConfig -> FacilityId -> IO (Maybe Facility)
fetchFacilityQ dbCfg facilityId =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runSelectReturningOne $
            lookup_ (_facility db) facilityId

deleteFacilityQ :: DBConfig -> Int32 -> IO ()
deleteFacilityQ dbCfg facilityId =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runDelete $
            delete (_facility db) (\f -> _facilityId f ==. val_ facilityId)

fetchAllFacilitiesQ :: DBConfig -> IO [Facility]
fetchAllFacilitiesQ dbCfg =
    withConnect (toConnectInfo dbCfg) $ \conn ->
        runBeamPostgres conn $ runSelectReturningList $
            select $ all_ (_facility db)

noBookingsWithinDatesQ :: DBConfig -> Int32 -> UTCTime -> UTCTime -> IO [Booking_]
noBookingsWithinDatesQ dbCfg facilityId startDate endDate = do
    withConnect (toConnectInfo dbCfg) $ \conn ->
        query conn [i|
            SELECT * FROM bookings
            WHERE facility_id = ? 
                AND date_time >= ? 
                AND date_time + book_for_how_many_hours <= ?
        |] (facilityId,startDate,endDate)

addHolidayForFacilitesQ :: DBConfig -> [FacilityId] -> UTCTime -> UTCTime -> Text -> IO ()
addHolidayForFacilitesQ dbCfg facilityIds startDate endDate typeOfHoliday = do
    withConnect (toConnectInfo dbCfg) $ \conn ->
        withTransaction conn $ mapM_ (\facilityId -> do
            runBeamPostgres conn $ runInsert $ insert (_holidayMaintainaceDays db) $
                insertExpressions [HolidayMaintainaceDays
                    (val_ facilityId)
                    (val_ startDate)
                    (val_ endDate)
                    (val_ typeOfHoliday)
                ]) facilityIds

fetchBookingQ :: DBConfig -> Int32 -> IO (Maybe Booking)
fetchBookingQ dbCfg bookingId =
    withConnect (toConnectInfo dbCfg) $ \conn ->
        runBeamPostgres conn $ runSelectReturningOne $
            lookup_ (_booking db) (BookingId bookingId)

confirmBookingQ :: DBConfig -> Int32 -> Int -> IO (Either String ())
confirmBookingQ dbCfg bookingId otp =
    withConnect (toConnectInfo dbCfg) $ \conn ->
        runBeamPostgres conn $ do
            mBooking <- runSelectReturningOne $ lookup_ (_booking db) (BookingId bookingId)
            case mBooking of
                Nothing -> pure $ Left "Booking does not exist"
                Just booking -> do
                        runUpdate $ save (_booking db) (booking {
                              _bookingStatus = "confirmed"
                            , _otp = Just $ T.pack (show otp)
                        })
                        pure $ Right ()

verifyBookingQ :: DBConfig -> Booking -> IO ()
verifyBookingQ dbCfg booking =
    withConnect (toConnectInfo dbCfg) $ \conn ->
        runBeamPostgres conn $ do
            runUpdate $ save (_booking db) (booking { _isVerified = True })

addToWaitlistQ :: DBConfig -> Int32 -> BookFacilityData -> IO (Maybe Int32)
addToWaitlistQ dbCfg userId BookFacilityData{..} = do
    waitlistRes <- withConnect (toConnectInfo dbCfg) $ \conn -> do
                    runBeamPostgres conn $ runInsertReturningList $ insert (_waitlist db) $
                        insertExpressions [
                        Waitlist
                            default_    --waitlistId
                            (val_ (UserId userId))
                            (val_ (FacilityId facilityId))
                            (val_ startTime)
                            (val_ endTime)
                            default_    --createdAt
                            default_    --updatedAt
                            ]
    case waitlistRes of
        [] -> pure Nothing
        (waitlist:_) -> pure $ Just (_waitlistId waitlist)

cancelBookingQ :: DBConfig -> Int32 -> IO (Either String ())
cancelBookingQ dbCfg bookingId =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        mBooking <- runBeamPostgres conn $ runSelectReturningOne $
                        lookup_ (_booking db) (BookingId bookingId)
        case mBooking of
            Nothing -> pure $ Left "Record with Booking Id does not exist"
            Just booking -> do
                runBeamPostgres conn $ runUpdate $
                    save (_booking db) booking { _bookingStatus = "cancelled" }
                pure $ Right ()

checkWaitlistQ :: DBConfig -> FacilityId -> UTCTime -> UTCTime -> IO (Maybe Waitlist)
checkWaitlistQ dbCfg facilityId startTime endTime =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runSelectReturningOne $
            select $ do
                orderBy_ (asc_ . fetchCreatedAt) $ do
                    waitlist <-  all_ (_waitlist db)
                    guard_ (_facilityIdForWaitlist waitlist ==. val_ facilityId)
                    guard_ (_preferredStartTime waitlist <=. val_ startTime
                            &&. _preferredEndTime waitlist >=. val_ endTime)
                    pure waitlist

fetchCreatedAt :: WaitlistT f -> Columnar f UTCTime
fetchCreatedAt Waitlist{..} = _createdAt

addWaitlistBookingQ :: DBConfig -> Int32 -> BookFacilityData -> Int32 -> IO ()
addWaitlistBookingQ dbCfg userId BookFacilityData{..} waitlistId = do
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        withTransaction conn $ do
                    runBeamPostgres conn $ runInsert $ insert (_booking db) $
                        insertExpressions [
                        Booking
                            default_
                            (val_ (UserId userId))
                            (val_ (FacilityId facilityId))
                            (val_ startTime)
                            (val_ "pending")
                            default_
                            default_
                            (val_ endTime)
                            default_
                            default_
                            ]
                    runBeamPostgres conn $ runDelete $ delete (_waitlist db) $
                        (\w -> _waitlistId w ==. val_ waitlistId)

addRatingQ :: DBConfig -> UserId -> FacilityId -> AddRatingData -> IO ()
addRatingQ dbCfg userId facilityId AddRatingData{..} =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runInsert $ insert (_rating db) $
            insertExpressions [
                Rating
                    default_ -- ratingId
                    (val_ userId)
                    (val_ facilityId)
                    (val_ rating)
                    (val_ review)
                    default_ --createdAt
                    default_ --updatedAt
            ]

checkIfUserUsedFacilityQ :: DBConfig -> UserId -> FacilityId -> IO (Maybe Booking)
checkIfUserUsedFacilityQ dbCfg userId facilityId =
    withConnect (toConnectInfo dbCfg) $ \conn -> do
        runBeamPostgres conn $ runSelectReturningFirst $
            select $ do
                booking <- all_ (_booking db)
                guard_ (_userIdForBooking booking ==. val_ userId)
                guard_ (_facilityIdForBooking booking ==. val_ facilityId)
                guard_ (_bookingStatus booking ==. val_ "confirmed")
                guard_ (_isVerified booking ==. val_ True)
                pure booking

{-
select *
from facility
where 
	(facility_name ilike '%tushar%' or facility_address ilike '%tushar%')
	and
	(facility_type = 'footballTurf')
	and
	(
		facility_id not in (
			select facility_id from bookings b 
			where
				(start_time <= (timestamp '2024-06-14 14:10:47.186 +0530')
				and end_time >= (timestamp '2024-06-14 14:10:47.186 +0530'))
				or
				(
				(start_time <= (timestamp '2024-06-14 15:30:47.186 +0530')
				and end_time >= (timestamp '2024-06-14 15:30:47.186 +0530'))
				)
		)
	)
;
-}
searchFacilityQ :: DBConfig -> SearchFacilityData -> IO [Facility]
searchFacilityQ dbCfg SearchFacilityData{..} = do
    withConnect (toConnectInfo dbCfg) $ \conn ->
        runBeamPostgres conn $ runSelectReturningList $
            select $
                filter_ (\f -> do
                    let x0 = case searchTerm of
                            Nothing -> val_ True
                            Just s ->
                                _facilityName f `ilike_` val_ ("%" <> s <> "%") ||.
                                        _facilityAddress f `ilike_` val_ ("%" <> s <> "%")
                        x1 = case facilityType of
                            Nothing -> val_ True
                            Just ft -> _facilityType f ==. val_ (facilityTypeToText ft)
                        x2 = case timeRange of
                            Nothing -> val_ True
                            Just timeRange_ -> case timeRange_ of
                                (start:end:_) -> do
                                    not_ $ exists_ $ do
                                      b <- all_ (_booking db)
                                      guard_ (_facilityIdForBooking b ==. FacilityId (_facilityId f))
                                      guard_ ((b._startTime <=. val_ start &&. b._endTime >=. val_ start) ||.
                                        (b._startTime <=. val_ end &&. b._endTime >=. val_ end))
                                      pure b
                                _ -> val_ True
                    x0 &&. x1 &&. x2
                ) $ do
                all_ (_facility db)

fetchAvailableSlotQuery :: Int32 -> Day -> Query
fetchAvailableSlotQuery facilityId date = [i|
     WITH facility_info AS (
    SELECT 
        f.facility_id,
        f.min_book_hour,
        f.facility_start_time,
        f.facility_end_time
    FROM 
        facility f
    WHERE 
        f.facility_id = #{facilityId}
),
booking_info AS (
    SELECT
        b.facility_id,
        b.start_time AS booking_start_time,
        b.end_time AS booking_end_time
    FROM 
        bookings b
    WHERE 
        b.facility_id = #{facilityId}
        AND DATE(b.start_time) = date '#{date}'
),
maintenance_info AS (
    SELECT
        m.facility_id,
        m.start_date AS maintenance_start_time,
        m.end_date AS maintenance_end_time
    FROM 
        holiday_maintainance_days m
    WHERE 
        m.facility_id = #{facilityId}
        AND DATE(m.start_date) = date '#{date}'
)
SELECT 
    fi.facility_id,
    fi.min_book_hour,
    fi.facility_start_time,
    fi.facility_end_time,
    bi.booking_start_time,
    bi.booking_end_time,
    mi.maintenance_start_time,
    mi.maintenance_end_time
FROM 
    facility_info fi
LEFT JOIN 
    booking_info bi ON fi.facility_id = bi.facility_id
LEFT JOIN 
    maintenance_info mi ON fi.facility_id = mi.facility_id;
|]

fetchAvailableSlotsQ :: DBConfig -> FetchAvailableSlotData -> IO [AvailableSlotData]
fetchAvailableSlotsQ dbCfg FetchAvailableSlotData{..} = do
    withConnect (toConnectInfo dbCfg) $ \conn ->
        query_ conn (fetchAvailableSlotQuery facilityId date)

topRatedFacilityQ :: DBConfig -> Text -> IO [TopRatingResult]
topRatedFacilityQ dbCfg fType = do
    withConnect (toConnectInfo dbCfg) $ \conn ->
        query_ conn [i|
            select f.facility_name, avg(r.rating) as avg_rating
            from facility f join ratings r on f.facility_id = r.facility_id
            where f.facility_type = '#{fType}'
            group by f.facility_id
            order by avg_rating desc
            limit 5
        |]

getBookingStatisticsQ :: DBConfig -> IO BookingStatistics
getBookingStatisticsQ dbCfg =
        withConnect (toConnectInfo dbCfg) $ \conn -> do
            BookingStatistics <$> mostBookedFacilityQ conn
                              <*> mostCancelledFacilityQ conn
                              <*> mostUnusedFacilityQ conn
                              <*> peakBookingHoursQ conn

mostBookedFacilityQ :: Connection -> IO MostBookedFacility
mostBookedFacilityQ conn = do
    facilities <- query_ conn [i|
        select f.facility_name
        from facility f join bookings b on f.facility_id = b.facility_id
        group by f.facility_id
        order by count(b.booking_id) desc
        limit 1
    |]
    case facilities of
        [] -> pure $ MostBookedFacility Nothing
        (facilityName:_) -> pure $ MostBookedFacility $ Just facilityName

mostCancelledFacilityQ :: Connection -> IO MostCancelledFacility
mostCancelledFacilityQ conn = do
    facilities <- query_ conn [i|
        select f.facility_name
        from facility f join bookings b on f.facility_id = b.facility_id
        where b.booking_status = 'cancelled'
        group by f.facility_id
        order by count(b.booking_id) desc
        limit 1
    |]
    case facilities of
        [] -> pure $ MostCancelledFacility Nothing
        (facilityName:_) -> pure $ MostCancelledFacility $ Just facilityName

mostUnusedFacilityQ :: Connection -> IO MostUnusedFacility
mostUnusedFacilityQ conn = do
    facilities <- query_ conn [i|
        select f.facility_name
        from facility f left join bookings b on f.facility_id = b.facility_id
        where b.booking_id is null
        group by f.facility_id
        order by count(b.booking_id) desc
        limit 1
    |]
    case facilities of
        [] -> pure $ MostUnusedFacility Nothing
        (facilityName:_) -> pure $ MostUnusedFacility $ Just facilityName

peakBookingHoursQ :: Connection -> IO (Maybe [PeekBookingHours])
peakBookingHoursQ conn = do
    res <- query_ conn [i|
        select date_trunc('hour', start_time) as booking_hour
        from bookings
        group by date_trunc('hour', start_time)
        order by count(booking_id) desc
        limit 5
    |] :: IO [PeekBookingHours]
    case res of
      [] -> pure Nothing
      _  -> pure $ Just res

findOverlappingSlotsQ :: DBConfig -> [UTCTime] -> [UTCTime] -> IO [(UTCTime,UTCTime)]
findOverlappingSlotsQ dbCfg startTimeSlot endTimeSlot = do
    withConnect (toConnectInfo dbCfg) $ \conn ->
        query conn [i|
            with input_slots as (SELECT unnest(?) as startslots
	,unnest(?) AS endslots)
SELECT 
  i.startslots,i.endslots
FROM 
  input_slots i
LEFT JOIN 
  bookings b ON (i.startslots :: timestamptz,i.endslots :: timestamptz) OVERLAPS (b.start_time, b.end_time)
LEFT JOIN 
  holiday_maintainance_days h ON (i.startslots :: timestamptz,i.endslots :: timestamptz) OVERLAPS (h.start_date, h.end_date)
WHERE 
  b.start_time IS NOT NULL OR h.start_date IS NOT NULL
ORDER BY 
  i.startslots;
        |] (PGArray startTimeSlot,PGArray endTimeSlot)

addSubscriptionQ :: DBConfig -> Int32 -> Int32 -> AddSubscriptionData -> IO ()
addSubscriptionQ dbCfg userId facilityId AddSubscriptionData{..} = do
    withConnect (toConnectInfo dbCfg) $ \conn ->
        runBeamPostgresDebug putStrLn conn $ runInsert $ insert (_bookingSubscription db) $
            insertExpressions [
                BookingSubscription 
                    default_ -- subId
                    (val_ $ UserId userId)
                    (val_ $ FacilityId facilityId)
                    (val_ $ dayToDate startDate)
                    (val_ $ dayToDate endDate)
                    (val_ startTime)
                    (val_ endTime)
                    (val_ intervalDays)
                    default_ --createdAt
            ]
