{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedRecordDot #-}
module LawnLogic.Platform.Handler where

import LawnLogic.Common.Model
import LawnLogic.Common.Types
import Control.Monad.Reader
import LawnLogic.Platform.DB.Query
import LawnLogic.Common.Utils
import Servant
import Data.Text (Text)

getFacilitiesH :: MonadIO m => AppT m [Facility]
getFacilitiesH = do
    cfg <- ask
    liftIO $ fetchAllFacilitiesQ (dbConfig cfg)

searchFacilityH :: MonadIO m
                => SearchFacilityData
                -> AppT m [Facility]
searchFacilityH searchFacilityData = do
    cfg <- ask
    fetchOrThrow (liftIO $ wrapQueryFunc 
                    (searchFacilityQ (dbConfig cfg) searchFacilityData))

fetchAvailableSlotH :: MonadIO m
                    => FetchAvailableSlotData
                    -> AppT m AvailableSlotResult_
fetchAvailableSlotH fetchAvailableSlotData= do
    cfg <- ask
    liftIO $ print fetchAvailableSlotData
    availableSlots <- fetchAvailableSlots (dbConfig cfg)
    case availableSlots of
        -- If no record return, then it must be because facilityId is wrong
        [] -> throwError $ err400 {errBody = "Record with given facilityId does not exist"}
        _  -> return $ go availableSlots
                
  where
    go availableSlots = do                   
        let result = fmap (\AvailableSlotData{..} -> AvailableSlotResult bookingStartTime bookingEndTime 
                            maintainanceStartTime maintainanceEndTime) availableSlots
            first = head availableSlots
            facilityInfo = AvailableSlotFacility first.minBookHour first.facilityStartTime first.facilityEndTime
        AvailableSlotResult_ facilityInfo result
    fetchAvailableSlots dbCfg = fetchOrThrow (liftIO $ wrapQueryFunc
                                                (fetchAvailableSlotsQ dbCfg fetchAvailableSlotData))

topRatedFacilityH :: MonadIO m
                  => Text 
                  -> AppT m [TopRatingResult]
topRatedFacilityH fType = do
    cfg <- ask
    fetchOrThrow (liftIO $ wrapQueryFunc (topRatedFacilityQ (dbConfig cfg) fType))
