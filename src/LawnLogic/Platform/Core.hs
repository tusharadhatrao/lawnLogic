{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DataKinds       #-}
module LawnLogic.Platform.Core (main,allServer) where
-- This module will be the entry point for the application.

import           Servant
import           LawnLogic.Common.Utils
import           LawnLogic.Common.Types
import           LawnLogic.Platform.API
import           Network.Wai.Handler.Warp (run)
import qualified Data.Text as T
import           Servant.Auth.Server
import           LawnLogic.Platform.Admin.Handler
import           LawnLogic.Platform.User.Handler
import           LawnLogic.Platform.Auth.Handler
import           LawnLogic.Platform.Handler
import           Control.Monad.Except (MonadIO)
import           Control.Monad.Trans.Reader (runReaderT)


server :: MonadIO m => CookieSettings -> JWTSettings -> ServerT (MainAPI auths) (AppT m)
server cookieSett jwtSett = adminProtectedHandlers 
                       :<|> addFacilityHandler 
                       :<|> updateFacilityHandler
                       :<|> createGroupHandler
                       :<|> removeFacilityFromGroupHandler
                       :<|> addFacilityToGroupHandler
                       :<|> deleteGroupHandler
                       :<|> deleteFacilityHandler
                       :<|> updateGroupFacilitiesHandler
                       :<|> setHolidayHandler
                       :<|> getBookingStatisticsHandler
                       :<|> exportStatisticsHandler
                       :<|> getFacilitiesHandler
                       :<|> adminLoginHandler
                       :<|> userRegisterHandler
                       :<|> userLoginHandler
                       :<|> searchFacilityHandler
                       :<|> fetchAvailableSlotHandler
                       :<|> topRatedFacilityHandler
                       :<|> userProfileHandler
                       :<|> userBookFacilityHandler
                       :<|> userPayBookingHandler
                       :<|> userVerifyBookingHandler
                       :<|> userWaitlistFacilityHandler
                       :<|> userCancelBookingHandler
                       :<|> userAddRatingHandler
                       :<|> userAddSubscriptionHandler
  where
    userAddSubscriptionHandler     = userAddSubscriptionH
    exportStatisticsHandler        = exportStatisticsH
    getBookingStatisticsHandler    = getBookingStatisticsH
    topRatedFacilityHandler        = topRatedFacilityH
    fetchAvailableSlotHandler      = fetchAvailableSlotH
    searchFacilityHandler          = searchFacilityH
    userAddRatingHandler           = userAddRatingH
    userCancelBookingHandler       = userCancelBookingH
    userWaitlistFacilityHandler    = userWaitlistFacilityH
    userVerifyBookingHandler       = userVerifyBookingH
    userPayBookingHandler          = userPayBookingH
    setHolidayHandler              = setHolidayH
    userBookFacilityHandler        = userBookFacilityH
    userProfileHandler             = userProfileH
    userLoginHandler               = userLoginH cookieSett jwtSett
    userRegisterHandler            = userRegisterH
    updateGroupFacilitiesHandler   = updateGroupFacilitiesH
    getFacilitiesHandler           = getFacilitiesH
    deleteGroupHandler             = deleteGroupH
    deleteFacilityHandler          = deleteFacilityH
    addFacilityToGroupHandler      = addFacilityToGroupH
    removeFacilityFromGroupHandler = removeFacilityFromGroupH
    createGroupHandler             = createGroupH
    updateFacilityHandler          = updateFacilityH
    addFacilityHandler             = addFacilityH
    adminProtectedHandlers         = dashboardH 
    adminLoginHandler              = adminLoginH cookieSett jwtSett

allServer :: Config -> JWTSettings -> Server (MainAPI auths) 
allServer cfg jwtSett = do
    hoistServerWithContext
        (Proxy :: Proxy (MainAPI '[JWT,Cookie]))
        (Proxy :: Proxy '[ CookieSettings, JWTSettings])
        (convertApp cfg)
        (server defaultCookieSettings jwtSett)

runApp :: Config -> IO ()
runApp cfg@Config{..} = do
    jwtSecretKey <- generateKey
    let jwtSett = defaultJWTSettings jwtSecretKey
        ctx = defaultCookieSettings :. jwtSett :. EmptyContext
    run appPort (app ctx jwtSett)
  where
    app ctx jwtSett = serveWithContext (Proxy :: Proxy (MainAPI '[JWT,Cookie]))
            ctx (allServer cfg jwtSett)

convertApp :: Config -> AppT IO a -> Handler a
convertApp cfg appt = Handler $ runReaderT (getApp appt) cfg

main :: IO ()
main = do
    config <- getConfig
    case config of
        Left err -> putStrLn $ "Error: " ++ T.unpack (unWrap err)
        Right cfg -> do
            putStrLn $ "Starting server on port " ++ show (appPort cfg)
            runApp cfg
