{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE QuasiQuotes #-}
{-# OPTIONS_GHC -Wno-name-shadowing #-}

module LawnLogic.Platform.Admin.Handler where

import           Servant
import           Servant.Auth.Server
import           Data.Text (Text)
import           LawnLogic.Common.Types
import           LawnLogic.Common.Model
import           LawnLogic.Common.Utils
import           Control.Monad.Reader
import           Data.ByteString.Lazy (ByteString)
import           LawnLogic.Platform.DB.Query
import           Data.Either
import           Text.Read
import           Data.Time
import           Control.Exception
import qualified Data.Text as T
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy.Char8 as BSC
import           System.FilePath ((</>))
import           GHC.Int
import           Data.String.Interpolate
import           Data.Csv hiding (Header)

throwErrorMessage :: MonadIO m => ServerError -> BSC.ByteString -> AppT m a
throwErrorMessage errStatus errMsg =  throwError $ errStatus { errBody = errMsg }

notAllowedErrorMessage :: MonadIO m => AppT m a
notAllowedErrorMessage = throwErrorMessage err403 "Only admins are allowed :("

cannotSetHolidayMessage :: BSC.ByteString
cannotSetHolidayMessage = [i|
    "Cannot set holiday, since confirmed bookings found on these dates"
|]

dashboardH :: MonadIO m => AuthResult Admin -> AppT m Text
dashboardH (Authenticated Admin{..}) = return $ "Welcome " <> _adminName <> "!"
dashboardH _ = notAllowedErrorMessage

textToDouble :: Text -> Maybe Double
textToDouble = readMaybe . T.unpack

textToTimeOfDay :: Text -> Maybe TimeOfDay
textToTimeOfDay = parseTimeM False defaultTimeLocale "%H:%M" . T.unpack

-- Given path of the temporary file, this function will move the file to the
-- desired location and return the path of the file, if successful.
-- Otherwise, it will return an error message. if the file contents
-- are not valid. i.e image
toFacilityImagePath :: Maybe (FilePath,TheFileType,TheFileName)
                    -> FilePath
                    -> IO (Either () (Maybe FilePath))
toFacilityImagePath (Just (filePath,fileType,fileName)) fileUploadDir = do
    let newFilePath = fileUploadDir </> T.unpack fileName
    if fileType /= "image/jpeg" && fileType /= "image/png" then pure $ Left ()
    else do
        fileContent <- catch (BS.readFile filePath)
            (\e -> print ("Error: " <> show (e :: SomeException)) >> pure BS.empty)
        if BS.null fileContent then pure $ Right Nothing
        else do
            catch (BS.writeFile newFilePath fileContent)
                (\e -> print ("Error: " <> show (e :: SomeException)))
            pure $ Right $ Just newFilePath
toFacilityImagePath Nothing _ = pure $ Right Nothing

toFacilityData :: FacilityMultipartData
                  -> FilePath
                  -> IO (Either ByteString FacilityData)
toFacilityData FacilityMultipartData{..} fileUploadDir = do
    let eFacilityType = toFacilityType factilityType
    let eMinBookHour = toHour minBookHour
    let eRentPerMinHour = toRentPerMinHour rentPerMinHour
    let eFacilityStartTime = toHour facilityStartTime
    let eFacilityEndTime = toHour facilityEndTime
    eFacilityImage <- toFacilityImagePath facilityImage fileUploadDir

    if      T.null facilityAddress    then pure $ Left "Facility address is required"
    else if T.null facilityName       then pure $ Left "Facility name is required"
    else if isLeft eFacilityType      then pure $ Left "Invalid facility type"
    else if isLeft eMinBookHour       then pure $ Left "Invalid min book hour"
    else if isLeft eRentPerMinHour    then pure $ Left "Invalid rent per min hour"
    else if isLeft eFacilityStartTime then pure $ Left "Invalid facility start time"
    else if isLeft eFacilityEndTime   then pure $ Left "Invalid facility end time"
    else if isLeft eFacilityImage     then pure $ Left "Invalid facility image"

    else pure $ Right FacilityData {
        facilityAddress     = facilityAddress
       , facilityName       = facilityName
       , minBookHour        = fromRight midday eMinBookHour
       , rentPerMinHour     = fromRight 0 eRentPerMinHour
       , facilityStartTime  = fromRight midnight eFacilityStartTime
       , facilityEndTime    = fromRight midnight eFacilityEndTime
       , facilityType       = fromRight FootballTurf eFacilityType
       , facilityImage      = fromRight Nothing eFacilityImage
    }
  where
    toFacilityType "tennisCourt" = Right TennisCourt
    toFacilityType "footballTurf" = Right FootballTurf
    toFacilityType "badmintonCourt" = Right BadmintonCourt
    toFacilityType _ = Left ()

    toRentPerMinHour rentPerMinHour_ = case textToDouble rentPerMinHour_ of
        Just x -> Right x
        Nothing -> Left ()

    toHour :: Text -> Either () TimeOfDay
    toHour x = case textToTimeOfDay x of
        Just x' -> Right x'
        Nothing -> Left ()

addFacilityH :: MonadIO m
                => AuthResult Admin
                -> FacilityMultipartData
                -> AppT m ()
addFacilityH (Authenticated _) facilityMultipartData = do
    cfg <- ask
    eRes <- liftIO $ toFacilityData facilityMultipartData (fileUploadDir cfg)
    case eRes of
        Left errMessage       -> throwError $ err400 {errBody = errMessage}
        Right addFacilityData -> liftIO $ addFacilityQ (dbConfig cfg) addFacilityData
addFacilityH _ _  = throwError $ err403 { errBody = "Only admins are allowed :(" }

-- updateFacilityH = undefined
updateFacilityH :: MonadIO m
                => AuthResult Admin
                -> Int32
                -> FacilityMultipartData
                -> AppT m ()
updateFacilityH (Authenticated _) facilityId facilityMultipartData = do
    cfg <- ask
    eRes <- liftIO $ toFacilityData facilityMultipartData (fileUploadDir cfg)
    case eRes of
        Left errMessage -> throwError $ err400 {errBody = errMessage}
        Right updateFacilityData -> do
            eRes <- liftIO $ updateFacilityQ
                (dbConfig cfg) (FacilityId $ fromIntegral facilityId) updateFacilityData
            case eRes of
                Left _ -> throwError $
                    err400 {errBody = "Facility with given id not found :("}
                Right _ -> return ()
updateFacilityH _ _ _ = throwError $ err403 { errBody = "Only admins are allowed :(" }

createGroupH :: MonadIO m
             => AuthResult Admin
             -> GroupData
             -> AppT m ()
createGroupH (Authenticated _) GroupData{..} = do
    cfg <- ask
    mGroupId <- liftIO $ insertGroupQ (dbConfig cfg) groupName
    case mGroupId of
        Nothing -> throwError $ err400 {errBody = "Group creation failed :("}
        Just groupId -> liftIO $
            insertFacilitiesGroupQ (dbConfig cfg) groupId facilityIds
createGroupH _ _ = throwError $ err403 { errBody = "Only admins are allowed :(" }

removeFacilityFromGroupH :: MonadIO m
                         => AuthResult Admin
                         -> Int32
                         -> Int32
                         -> AppT m ()
removeFacilityFromGroupH (Authenticated _) groupId facilityId = do
    cfg <- ask
    mFacilityGroup <- liftIO $ fetchFacilityGroupQ (dbConfig cfg) groupId facilityId
    case mFacilityGroup of
        Nothing -> throwError $ err400 {errBody = "Groupid or facilityId does not exist!"}
        Just _ -> liftIO $ deleteFacilityGroupQ (dbConfig cfg) groupId facilityId
removeFacilityFromGroupH _ _ _ = notAllowedErrorMessage


addFacilityToGroupH :: MonadIO m
                    => AuthResult Admin
                    -> Int32
                    -> Int32
                    -> AppT m ()
addFacilityToGroupH (Authenticated _) groupId facilityId = do
    cfg <- ask
    mFacilityGroup <- liftIO $ fetchFacilityGroupQ (dbConfig cfg) groupId facilityId
    case mFacilityGroup of
        Nothing -> do
           eRes <- liftIO $ catch (do
                addFacilityGroupQ (dbConfig cfg) groupId facilityId
                pure $ Right ())
                        (\(e :: SomeException) -> pure $ Left e)
           case eRes of
             Left e -> throwError $ err400 {errBody = BSC.pack $ show e}
             Right _ -> return ()

        Just _ -> throwError $ err400 {errBody = "Groupid or facilityId already exist!"}
addFacilityToGroupH _ _ _ = notAllowedErrorMessage

deleteGroupH :: MonadIO m
             => AuthResult Admin
             -> Int32
             -> AppT m ()
deleteGroupH (Authenticated _) groupId = do
    cfg <- ask
    mGroup <- liftIO $ fetchGroupQ (dbConfig cfg) groupId
    case mGroup of
        Nothing -> throwErrorMessage err400 "Record id with given group id not found!"
        Just _  -> liftIO $ deleteGroupQ (dbConfig cfg) groupId
deleteGroupH _ _ = notAllowedErrorMessage

deleteFacilityH :: MonadIO m
             => AuthResult Admin
             -> Int32
             -> AppT m ()
deleteFacilityH (Authenticated _) facilityId = do
    cfg <- ask
    mFacility <- liftIO $ fetchFacilityQ (dbConfig cfg) (FacilityId facilityId)
    case mFacility of
        Nothing -> throwErrorMessage err400 "Record id with given group id not found!"
        Just _  -> liftIO $ deleteFacilityQ (dbConfig cfg) facilityId
deleteFacilityH _ _ = notAllowedErrorMessage

toUpdateGroupData :: UpdateGroupMultipartData -> FilePath -> IO UpdateGroupData
toUpdateGroupData UpdateGroupMultipartData{..} fileUploadDir = do
    let eFacilityType = case facilityType of
            Just x -> case x of
                "tennisCourt" -> Right $ Just TennisCourt
                "footballTurf" -> Right $ Just FootballTurf
                "badmintonCourt" -> Right $ Just BadmintonCourt
                _ -> Left ()
            Nothing -> Left ()
    let eMinBookHour = case minBookHour of
            Just x -> case textToTimeOfDay x of
                Just x' -> Right $ Just x'
                Nothing -> Left ()
            Nothing -> Left ()
    let eRentPerMinHour = case rentPerMinHour of
            Just x -> case textToDouble x of
                Just x' -> Right $ Just x'
                Nothing -> Left ()
            Nothing -> Left ()
    let eFacilityStartTime = case facilityStartTime of
            Just x -> case textToTimeOfDay x of
                Just x' -> Right $ Just x'
                Nothing -> Left ()
            Nothing -> Left ()
    let eFacilityEndTime = case facilityEndTime of
            Just x -> case textToTimeOfDay x of
                Just x' -> Right $ Just x'
                Nothing -> Left ()
            Nothing -> Left ()
    eFacilityImage <- toFacilityImagePath facilityImage fileUploadDir

    pure $ UpdateGroupData {
        facilityType       = fromRight Nothing eFacilityType
    , minBookHour        = fromRight Nothing eMinBookHour
    , facilityAddress    = facilityAddress
    , rentPerMinHour     = fromRight Nothing eRentPerMinHour
    , facilityStartTime  = fromRight Nothing eFacilityStartTime
    , facilityEndTime    = fromRight Nothing eFacilityEndTime
    , facilityImage      = fromRight Nothing eFacilityImage
    }

updateGroupFacilitiesH :: MonadIO m
                    => AuthResult Admin
                    -> Int32
                    -> UpdateGroupMultipartData
                    -> AppT m ()
updateGroupFacilitiesH (Authenticated _) groupId updateGroupMultipartData = do
    cfg <- ask
    let dbCfg = dbConfig cfg
    facilities <- liftIO $ fetchGroupFacilitiesQ dbCfg groupId
    case facilities of
        [] -> throwErrorMessage err400 "No facilities associated with given group were found!"
        _ -> liftIO $ updateFacilitiesQ dbCfg facilities =<< liftIO
                (toUpdateGroupData updateGroupMultipartData (fileUploadDir cfg))
updateGroupFacilitiesH _ _ _ = notAllowedErrorMessage

setHolidayH :: MonadIO m
            => AuthResult Admin
            -> SetFacilityHolidayData
            -> AppT m ()
setHolidayH (Authenticated _) SetFacilityHolidayData{..} = do
    cfg <- ask
    let dbCfg = dbConfig cfg
    eRes <- liftIO $ wrapQueryFunc (noBookingsWithinDatesQ dbCfg facilityId startDate endDate)
    case eRes of
        Left e -> throwError $ err400 {errBody = BSC.pack $ show e}
        Right res ->
            case res of
                [] -> runOrThrow (liftIO $
                    wrapQueryFunc (addHolidayForFacilitesQ
                        dbCfg [FacilityId facilityId] startDate endDate typeOfHoliday))
                _  -> throwError $ err400 {errBody = cannotSetHolidayMessage }
    where
        runOrThrow func = do
            eRes <- func
            case eRes of
                Left e -> throwError $ err400 {errBody = BSC.pack $ show e}
                Right _ -> pure ()
setHolidayH _ _ = notAllowedErrorMessage


getBookingStatisticsH :: MonadIO m 
                      => AuthResult Admin
                      -> AppT m BookingStatistics
getBookingStatisticsH (Authenticated _) = do
    cfg <- ask
    fetchOrThrow (liftIO $ wrapQueryFunc (getBookingStatisticsQ (dbConfig cfg)))
getBookingStatisticsH _ = notAllowedErrorMessage

exportStatisticsH :: MonadIO m
                  => AuthResult Admin
                  -> AppT m (Headers '[Header "Content-Disposition" String] ByteString)
exportStatisticsH (Authenticated _) = do
    cfg <- ask
    result <- fetchOrThrow (liftIO $ wrapQueryFunc (getBookingStatisticsQ (dbConfig cfg)))
    return $ addHeader "attachment; filename=booking_statistics.csv" $ encodeDefaultOrderedByName [result]
exportStatisticsH _ = notAllowedErrorMessage
