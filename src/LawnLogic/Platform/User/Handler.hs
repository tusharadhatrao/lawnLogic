{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module LawnLogic.Platform.User.Handler where

import           Control.Monad.Reader
import           Servant.Auth.Server
import           Servant
import           LawnLogic.Common.Model
import           LawnLogic.Common.Types
import           LawnLogic.Platform.Utils
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.String.Interpolate
import           Data.ByteString (ByteString)
import qualified Data.ByteString.Lazy.Char8 as BSC
import           LawnLogic.Platform.DB.Query
import           Data.Time.Clock
import           GHC.Int
import           LawnLogic.Common.Utils
import           System.Random
import           Data.Maybe
import           Data.Time

facilityNotFoundError :: MonadIO m => AppT m a
facilityNotFoundError = throwError $ err400 { errBody = "record with given facility Id does not exist"}

fetchFacility :: MonadIO m => Config -> Int32 -> AppT m (Maybe Facility)
fetchFacility cfg facilityId = fetchOrThrow (liftIO $ wrapQueryFunc 
                            (fetchFacilityQ (dbConfig cfg) (FacilityId facilityId)))

facilityNotAvailableMsg :: ByteString
facilityNotAvailableMsg = [i|
            Sorry Facility is not available at requested timeslot, 
            please pick other slot or apply for waitlist.
            |]

userProfileH :: MonadIO m => AuthResult User -> AppT m Text
userProfileH (Authenticated User{..}) = pure $ "Hello " <> _userName <> "!"
userProfileH _ = throwError $ err401 {errBody = "Please login first!"}

userBookFacilityH :: MonadIO m
                  => AuthResult User
                  -> BookFacilityData
                  -> AppT m BookingInitiatedInfo
userBookFacilityH (Authenticated User{..}) bookingFacilityData@BookFacilityData{..} = do
    cfg <- ask
    mFacility <- liftIO $ fetchFacilityQ (dbConfig cfg) (FacilityId facilityId)
    case mFacility of
      Just Facility{..} -> do
        if isTimeSlotAligned _minBookHour startTime endTime then do
            isFacilityAvailable <- liftIO $ checkIfFacilityAvailable (dbConfig cfg) facilityId
                                            startTime endTime
            if isFacilityAvailable then do
              eBookingId <- addUnconfirmedBooking cfg
              case eBookingId of
                Right mBookingId -> do
                         case mBookingId of
                            Nothing -> genericError
                            Just bookingId -> pure $ BookingInitiatedInfo {
                                    bookingId = bookingId
                                  , message = "Your booking processed initiated, please pay amount to confirm booking"
                                  , paymentAmount = calculateAmount _rentPerMinBookHour _minBookHour startTime endTime
                                }
                Left e -> errorWithArg e
            else notAvailableError
        else minBookHourNotMatching
      Nothing -> facilityDoesNotExistError
  where
    genericError = throwError $ err401 {errBody = "something went wrong, please try again!"}
    errorWithArg e = throwError $ err405 {errBody = BSC.pack $ "bad" <> show e}
    notAvailableError = throwError $ err400 {errBody = "Sorry facility is not available for given timeslot,"}
    minBookHourNotMatching = throwError $ err400 
        {errBody = "start and endtime should be in moldulo with min_book_hour"}
    facilityDoesNotExistError = throwError $ err400 { errBody = "Bad request, facilityId does not exist" }
    
    addUnconfirmedBooking cfg = liftIO $ wrapQueryFunc $ addUnconfirmedBookingQ 
                                (dbConfig cfg) _userId bookingFacilityData

userBookFacilityH _ _ = throwError $ err401 { errBody = "Please login first :(" }

userPayBookingH :: MonadIO m
                => AuthResult User
                -> PayBookingData
                -> AppT m Text
userPayBookingH (Authenticated User{..}) payBookingData@PayBookingData{..} = do
    cfg <- ask
    liftIO $ putStrLn "Fetching booking"
    emBooking <- liftIO $ wrapQueryFunc (fetchBookingQ (dbConfig cfg) (fetchBookingId payBookingData))
    case emBooking of
        Left e -> throwError $ err400 {errBody = BSC.pack $ show e}
        Right mBooking -> do
                case mBooking of
                    Nothing -> throwError $ err400 {errBody = "Booking Id does not exist!"}
                    Just booking@Booking{..} -> do
                        if _userIdForBooking /= UserId _userId then
                            throwError $ err400 {errBody = "Please pay with correct user loggedIn"}
                        else do
                            eRes0 <- liftIO $ validateAmount 
                                    (dbConfig cfg) payingAmount _startTime _endTime (fetchFacilityId booking)
                            case eRes0 of
                                Left e -> throwError $ err400 { errBody = BSC.pack $ show e }
                                Right _ -> do
                                    otp <- liftIO $ randomRIO @Int (1000,9999)
                                    eRes <- liftIO $ confirmBookingQ (dbConfig cfg) (fetchBookingId payBookingData) otp
                                    case eRes of
                                        Left e -> throwError $ err400 {errBody = BSC.pack $ show e}
                                        Right _ -> pure $
                                            "Your booking has been confirmed! here's your opt: " <> T.pack (show otp)
userPayBookingH _ _ = throwError $ err401 { errBody = "Please login first :(" }

userVerifyBookingH :: MonadIO m
                   => AuthResult User
                   -> Int32
                   -> VerifyBookingData
                   -> AppT m Text
userVerifyBookingH (Authenticated User{..}) bookingId VerifyBookingData{..} = do
    cfg <- ask
    mBookingId <- fetchOrThrow (liftIO $ (wrapQueryFunc (fetchBookingQ (dbConfig cfg) bookingId)))
    case mBookingId of
        Nothing -> recordDoesNotExists 
        Just booking@Booking{..} -> do
            if((UserId _userId) /=  _userIdForBooking) then userNotMatchedError
            else
                if((Just $ T.pack $ show otp) /= _otp) then wrongOTPError
                else do
                    currentTime <- liftIO getCurrentTime
                    if(_startTime > plusTenMinutes currentTime) then tooEarlyVerifyError
                    else do
                        _ <- fetchOrThrow (liftIO $ wrapQueryFunc (verifyBookingQ (dbConfig cfg) booking))
                        pure $ "Booking Verified! Enjoy the game :)"
  where
    recordDoesNotExists = throwError $ err400 {errBody = "Record with given booking Id does not exists."}
    userNotMatchedError = throwError $ err405  {errBody = "Please login with correct User"}
    wrongOTPError       = throwError $ err406 {errBody = "Wrong OTP :("} 
    tooEarlyVerifyError = throwError $ err400 {errBody = "Please verify OTP 10 minutes before the start time"}

userVerifyBookingH _ _ _ = throwError $ err401 {errBody = "Please login first :("}

userWaitlistFacilityH :: MonadIO m
                      => AuthResult User
                      -> BookFacilityData
                      -> AppT m Text
userWaitlistFacilityH (Authenticated User{..}) bookFacilityData@BookFacilityData{..} = do
    cfg <- ask
    mFacility <- fetchFacility cfg facilityId
    case mFacility of
        Nothing -> facilityNotFoundError
        Just _ -> do
            isFacilityAvailable <- checkIfFacilityAvailable' cfg
            if isFacilityAvailable then facilityIsAvailableError
            else do
                res <- addToWaitlist cfg
                pure $ ("Booking has been added to waitlist :) waitListId: " <> (T.pack $ show res))
  where
    facilityIsAvailableError = throwError $ err400 { errBody = "cannot waitlist, facility is available to book" }
    checkIfFacilityAvailable' cfg = null <$> fetchOrThrow (liftIO $ wrapQueryFunc 
                            (checkIfFacilityAvailableQ (dbConfig cfg) facilityId startTime endTime))
    addToWaitlist cfg = fetchOrThrow (liftIO $ wrapQueryFunc 
                            (addToWaitlistQ (dbConfig cfg) _userId bookFacilityData))

userWaitlistFacilityH _ _ = throwError $ err401 { errBody = "Please login first :(" }
 
userCancelBookingH :: MonadIO m
                   => AuthResult User
                   -> Int32
                   -> AppT m Text
userCancelBookingH (Authenticated User{..}) bookingId = do
    cfg <- ask
    mBooking <- fetchBooking cfg
    case mBooking of
        Nothing -> bookingNotFoundError
        Just Booking{..} -> do
            if(_userIdForBooking /= UserId _userId) then userIdNotMatchedError
            else do
                void $ cancelBooking cfg
                checkAndApplyWaitlist cfg _facilityIdForBooking _startTime _endTime
                pure "Your booking has been cancelled, sad to see you go :("
  where
     bookingNotFoundError = throwError $ err400 { errBody = "Record with given booking id does not exist." }
     userIdNotMatchedError = throwError $ err400 {errBody =  "Please login with user who booked facility and cancel it."}
     fetchBooking cfg = fetchOrThrow (liftIO $ wrapQueryFunc
                            (fetchBookingQ (dbConfig cfg) bookingId))
     cancelBooking cfg = fetchOrThrow (liftIO $ wrapQueryFunc
                            (cancelBookingQ (dbConfig cfg) bookingId))
     checkAndApplyWaitlist cfg facilityId startTime endTime = do
        mWaitlist <- fetchOrThrow (liftIO $ wrapQueryFunc 
            (checkWaitlistQ (dbConfig cfg) facilityId startTime endTime))
        case mWaitlist of
            Nothing -> pure ()
            Just Waitlist{..} -> do
                let bookFacilityData = BookFacilityData {
                    facilityId = fetchFacilityIdNum _facilityIdForWaitlist 
                  , startTime  = _preferredStartTime
                  , endTime    = _preferredEndTime
                }
                _ <- fetchOrThrow (liftIO $ wrapQueryFunc
                    (addWaitlistBookingQ (dbConfig cfg) 
                        (fetchUserId _userIdForWaitlist) bookFacilityData _waitlistId))
                pure ()
                -- here there should be a function that sends the user notification/email about booking.
        pure ()

userCancelBookingH _ _ = throwError $ err401 { errBody = "Please login first :(" } 

userAddRatingH :: MonadIO m
                => AuthResult User
                -> Int32
                -> AddRatingData
                -> AppT m Text
userAddRatingH (Authenticated User{..}) facilityId addRatingData@AddRatingData{..} = do
    cfg <- ask
    if rating > 10 || rating < 0 then ratingNotGoodError
    else do
        mFacility <- fetchFacility cfg facilityId
        case mFacility of
            Nothing -> facilityDoesNotExistError
            Just _  -> do
                hasUsed <- hasUserUsedFacility (dbConfig cfg)
                if not hasUsed then userNotUsedFacilityError
                else do
                    addRating (dbConfig cfg)
                    pure "Rating has been added. Thank you for your feedback :)"
  where
    ratingNotGoodError = throwError $ err400 {errBody = "Rating must be between 0 to 10"}
    facilityDoesNotExistError = throwError $ err400 {errBody = "Facility with given Id does not exist"}
    userNotUsedFacilityError = throwError $ err400 {errBody = "Only the users who used the facility can give rating."}

    hasUserUsedFacility dbCfg = do
            liftIO $ print (_userId,facilityId)
            res <- fetchOrThrow (liftIO $ wrapQueryFunc 
                                    (checkIfUserUsedFacilityQ dbCfg (UserId _userId) (FacilityId facilityId)))
            liftIO $ print res
            pure $ isJust res
    addRating dbCfg = fetchOrThrow (liftIO $ wrapQueryFunc 
                            (addRatingQ dbCfg (UserId _userId) (FacilityId facilityId) addRatingData))

userAddRatingH _ _ _ = throwError $ err401 {errBody = "Please login first!"}

type Timeslot = (UTCTime,UTCTime)

userAddSubscriptionH :: MonadIO m
                     => AuthResult User
                     -> Int32
                     -> AddSubscriptionData
                     -> AppT m Text
userAddSubscriptionH (Authenticated User{..}) facilityId a@AddSubscriptionData{..} = do
    cfg <- ask
    mFacility <- fetchFacility cfg facilityId
    case mFacility of
        Nothing -> facilityNotFoundError 
        Just _  -> do
            let (startTimeSlots,endTimeSlots) = createListOfTimestamps
            overlappingSlots <- findOverlappingSlots (dbConfig cfg) startTimeSlots endTimeSlots
            let filteredSlots = filter (\x -> any (x==) overlappingSlots) (zip startTimeSlots endTimeSlots)
            addSubscription (dbConfig cfg)
            pure ("Subscription confirmed! Following slots cannot be booked:" <> (T.pack $ show filteredSlots))
  where
    addSubscription dbCfg = fetchOrThrow (liftIO $ wrapQueryFunc
                                (addSubscriptionQ dbCfg _userId facilityId a))
    createListOfTimestamps :: ([UTCTime],[UTCTime])
    createListOfTimestamps = do
        let startDiffTime       = timeOfDayToTime startTime
            endDiffTime         = timeOfDayToTime endTime
            startTimeslots      = go startDiffTime startDate 
            endTimeslots        = go endDiffTime startDate 
        (startTimeslots,endTimeslots)
    go diffTime currDate = do
        if (endDate >= currDate) then 
            (UTCTime currDate diffTime) : 
                go diffTime (addDays intervalDays currDate)
        else []
    findOverlappingSlots dbCfg startTimeSlot endTimeSlot = fetchOrThrow (liftIO $ wrapQueryFunc 
                                    (findOverlappingSlotsQ dbCfg startTimeSlot endTimeSlot))


userAddSubscriptionH _ _ _ = throwError $ err401 {errBody = "Please login first!"}

checkIfFacilityAvailable :: DBConfig -> Int32 -> UTCTime -> UTCTime -> IO Bool
checkIfFacilityAvailable dbCfg facilityId startTime endTime = do
    null <$> checkIfFacilityAvailableQ dbCfg facilityId startTime endTime

validateAmount :: DBConfig
               -> Double
               -> UTCTime
               -> UTCTime
               -> FacilityId
               -> IO (Either BSC.ByteString ())
validateAmount dbCfg payingAmount startTime endTime facilityId = do
    mFacility <- liftIO $ fetchFacilityQ dbCfg facilityId
    case mFacility of
        Nothing -> pure $ Left "Facility does not exist"
        Just Facility{..} -> do
            let amount = calculateAmount _rentPerMinBookHour _minBookHour startTime endTime
            if amount == payingAmount then
                pure $ Right ()
            else
                pure $ Left "Amount does not match"

