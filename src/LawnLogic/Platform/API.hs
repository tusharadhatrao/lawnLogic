{-# LANGUAGE DataKinds #-}
module LawnLogic.Platform.API where

import Servant
import Servant.Auth.Server
import Data.Text (Text)
import LawnLogic.Common.Types
import LawnLogic.Common.Model
import Servant.Multipart
import GHC.Int
import Data.ByteString.Lazy (ByteString)

type MainAPI auths =     Auth auths Admin :> AdminProtectedAPI
                    :<|> Auth auths Admin :> AddFacilityAPI                                    
                    :<|> Auth auths Admin :> UpdateFacilityAPI                                    
                    :<|> Auth auths Admin :> CreateGroupAPI
                    :<|> Auth auths Admin :> RemoveFacilityFromGroupAPI
                    :<|> Auth auths Admin :> AddFacilityToGroupAPI
                    :<|> Auth auths Admin :> DeleteGroupAPI 
                    :<|> Auth auths Admin :> DeleteFacilityAPI
                    :<|> Auth auths Admin :> UpdateGroupFacilitiesAPI
                    :<|> Auth auths Admin :> SetHolidayAPI
                    :<|> Auth auths Admin :> BookingStatisticsAPI
                    :<|> Auth auths Admin :> ExportStatitsticsAPI
                    :<|> FetchFacilitiesAPI
                    :<|> AdminLoginAPI
                    :<|> UserRegisterAPI
                    :<|> UserLoginAPI
                    :<|> SearchFacilityAPI
                    :<|> FindAvailableSlotAPI
                    :<|> TopRatedFacilityAPI
                    :<|> Auth auths User :> UserProfileAPI
                    :<|> Auth auths User :> BookFacilityAPI
                    :<|> Auth auths User :> PayBookingAmountAPI
                    :<|> Auth auths User :> VerifyBookingAPI
                    :<|> Auth auths User :> WaitlistFacilityAPI
                    :<|> Auth auths User :> CancelBookingAPI
                    :<|> Auth auths User :> AddRatingAPI
                    :<|> Auth auths User :> AddSubscriptionAPI

type AddSubscriptionAPI = "user"
                :> "add_subscription"
                    :> Capture "facilityId" Int32
                        :> ReqBody '[JSON] AddSubscriptionData
                            :> Post '[JSON] Text

type ExportStatitsticsAPI = "admin"
                :> "export_statistics"
                    :> Get '[OctetStream] (Headers '[Header "Content-Disposition" String] ByteString)

type BookingStatisticsAPI = "admin"
            :>"booking_statistics"
                :> Get '[JSON] BookingStatistics

type TopRatedFacilityAPI = "top_rated_facility"
                :> Capture "facility_type" Text
                    :> Get '[JSON] [TopRatingResult]

type FindAvailableSlotAPI = "find_slot"
                :> ReqBody '[JSON] FetchAvailableSlotData
                    :> Get '[JSON] AvailableSlotResult_

type SearchFacilityAPI = "search_facility"
                :> ReqBody '[JSON] SearchFacilityData
                    :> Get '[JSON] [Facility]

type AddRatingAPI = "user"
                :> "add_rating"
                    :> Capture "facilityId" Int32
                        :> ReqBody '[JSON] AddRatingData
                            :> Post '[JSON] Text

type CancelBookingAPI = "user"
                :> "cancel_booking"
                    :> Capture "booking_id" Int32
                        :> Delete '[JSON] Text

type WaitlistFacilityAPI = "user"
                :> "add_waitlist"
                    :> ReqBody '[JSON] BookFacilityData
                        :> Post '[JSON] Text

type VerifyBookingAPI = "user"
                :> "verify_booking"
                    :> Capture "booking_id" Int32
                        :> ReqBody '[JSON] VerifyBookingData
                            :> Post '[JSON] Text 

type PayBookingAmountAPI = "user"
                :> "pay_booking"
                    :> ReqBody '[JSON] PayBookingData
                        :> Post '[JSON] Text

type SetHolidayAPI = "admin"
                :> "set_holiday"
                    :> "facility"
                        :> ReqBody '[JSON] SetFacilityHolidayData
                            :> Post '[JSON] ()

type BookFacilityAPI = "user"
                :> "book_facility"
                    :> ReqBody '[JSON] BookFacilityData
                        :> Post '[JSON] BookingInitiatedInfo

type UserProfileAPI = "user"
                :> "profile"
                    :> Get '[JSON] Text

type UserLoginAPI = "user"
                :> "login"
                    :> ReqBody '[JSON] UserLoginData
                        :> Post '[JSON] (Headers
                                    '[Header "Set-Cookie" SetCookie
                                        , Header "Set-Cookie" SetCookie] Text)

type UserRegisterAPI = "user"
                :> "register"
                    :> ReqBody '[JSON] UserRegisterData
                        :> Post '[JSON] ()

type UpdateGroupFacilitiesAPI = "admin"
                :> "group"
                    :> Capture "group_id" Int32
                        :> "all_update"
                            :> MultipartForm Tmp UpdateGroupMultipartData
                                :> Post '[JSON] ()

type FetchFacilitiesAPI = "facilities" 
                :> Get '[JSON] [Facility]

type DeleteGroupAPI = "admin"
                :> "delete_group"
                    :> Capture "group_id" Int32
                        :> Delete '[JSON] ()

type DeleteFacilityAPI = "admin"
                :> "delete_facility"
                    :> Capture "facility_id" Int32
                        :> Delete '[JSON] ()

type AddFacilityToGroupAPI = "admin" 
                :> "update_group" 
                    :> Capture "group_id" Int32
                        :> "add_facility" 
                            :> Capture "facility_id" Int32
                                :> Put '[JSON] ()

type RemoveFacilityFromGroupAPI = "admin" 
                :> "update_group" 
                    :> Capture "group_id" Int32
                        :> "remove_facility" 
                            :> Capture "facility_id" Int32
                                :> Put '[JSON] ()
type CreateGroupAPI = "admin" 
                :> "create_group" 
                    :> ReqBody '[JSON] GroupData 
                        :> Post '[JSON] ()

type AdminProtectedAPI = "admin" 
                :> "dashboard" 
                    :> Get '[JSON] Text

type AddFacilityAPI = "admin" 
                :> "add_facility" 
                    :>  MultipartForm Tmp FacilityMultipartData 
                        :> Post '[JSON] ()

type UpdateFacilityAPI = "admin" 
                    :> "update_facility"
                        :> Capture "facilityId" Int32
                            :> MultipartForm Tmp FacilityMultipartData
                                :> Put '[JSON] ()

type AdminLoginAPI = "admin" :> "login" :> ReqBody '[JSON] AdminLoginData
                            :> Post '[JSON] (Headers
                                '[Header "Set-Cookie" SetCookie
                                    , Header "Set-Cookie" SetCookie] Text) 
