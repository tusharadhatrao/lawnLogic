# Revision history for lawnLogic

## 0.1.0.0 -- YYYY-mm-dd

* First version. Released on an unsuspecting world.

## 31/05/2024

* Added Admin login functionality.
* Added ReaderT monad to pass dbConfigs.